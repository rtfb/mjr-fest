if (document.images)
{
    var btnUnchecked = new Image ();
    btnUnchecked.src = window.js2wv.getImgLoc () + "checkbox-grey.png";
    var btnChecked = new Image ();
    btnChecked.src = window.js2wv.getImgLoc () + "checkbox-green.png";
}

function clickCheckbox (btnNo)
{
    if (!document.images)
        return;

    var btn = document.getElementById ("btn_" + btnNo);
    var isChecked = btn.src.indexOf ("checkbox-green.png") != -1;

    if (!isChecked)
    {
        btn.src = btnChecked.src;
    }
    else
    {
        btn.src = btnUnchecked.src;
    }

    window.js2wv.toggleCheckbox (btnNo, !isChecked, document.body.scrollTop);
}

function restoreScrollPos (pos)
{
    document.body.scrollTop = pos;
}

function saveScrollPos() {
    window.js2wv.saveScrollPos(document.body.scrollTop);
}
