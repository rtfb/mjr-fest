package lt.rtfb.fest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

// from here: http://www.androidsnippets.com/drawing-an-image-as-a-map-overlay2
public class DrawableMapOverlay extends Overlay
{
    private final GeoPoint mGeoPoint;
    private boolean mFirstDraw = true;
    private double mLatitude = 0;
    private Rect mSrcRect = null;
    private Rect mDstRect = new Rect ();
    private Bitmap mMarkerImage = null;
    private Point mScreenPoint = new Point ();

    // @param context the context in which to display the overlay
    // @param geoPoint the geographical point where the overlay is located
    // @param drawable the ID of the desired drawable
    public DrawableMapOverlay (Context context, GeoPoint geoPoint, int drawable)
    {
        mGeoPoint = geoPoint;

        // Read the image
        Resources res = context.getResources ();
        mMarkerImage = BitmapFactory.decodeResource (res, drawable);
        mSrcRect = new Rect (0, 0, mMarkerImage.getWidth (),
                             mMarkerImage.getHeight ());
        mLatitude = (double)mGeoPoint.getLatitudeE6 () / 1000000;
    }

    @Override
    public boolean draw (Canvas canvas, MapView mapView,
                         boolean shadow, long when)
    {
        super.draw (canvas, mapView, shadow);

        if (mFirstDraw)
        {
            mapView.getController ().setZoom (15);
            mapView.getController ().setCenter (mGeoPoint);
            mFirstDraw = false;
        }

        // Convert geo coordinates to screen pixels
        mapView.getProjection ().toPixels (mGeoPoint, mScreenPoint);

        double overlayRadius = metersToRadius (1000, mapView, mLatitude);
        mDstRect.set (mScreenPoint.x,
                      mScreenPoint.y,
                      (int) (mScreenPoint.x + overlayRadius),
                      (int) (mScreenPoint.y + overlayRadius));
        canvas.drawBitmap (mMarkerImage, mSrcRect, mDstRect, null);

        return true;
    }

    private double metersToRadius (float meters, MapView map, double latitude)
    {
        return (map.getProjection ().metersToEquatorPixels (meters)
                * (1 / Math.cos (Math.toRadians (latitude))));
    }

    @Override
    public boolean onTap (GeoPoint p, MapView mapView)
    {
        // Handle tapping on the overlay here
        return true;
    }
}
