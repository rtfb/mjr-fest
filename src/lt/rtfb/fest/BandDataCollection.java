package lt.rtfb.fest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class BandDataCollection<I extends SelfHashable>
                    implements Iterable<BandData>
{
    private LinkedHashMap<String, I> mMap;

    public BandDataCollection ()
    {
        mMap = new LinkedHashMap<String, I> ();
    }

    public BandDataCollection(I[] arr) {
        this();

        if (arr == null)
            return;

        for (I bd : arr)
            put(bd);
    }

    public void put(I item)
    {
        mMap.put (item.getId (), item);
    }

    public void add (BandDataCollection<I> other)
    {
        mMap.putAll (other.mMap);
    }

    public I remove (String key)
    {
        return mMap.remove (key);
    }

    public I get (String key)
    {
        return mMap.get (key);
    }

    public boolean contains (I obj)
    {
        return mMap.get (obj.getId ()) != null;
    }

    public boolean isEmpty ()
    {
        if (mMap == null)
            return true;

        return mMap.isEmpty ();
    }

    public int size ()
    {
        if (mMap == null)
            return 0;

        return mMap.values ().size ();
    }

    public Collection<String> keys ()
    {
        return mMap.keySet ();
    }

    public Iterator<BandData> iterator ()
    {
        Collection<BandData> coll = (Collection<BandData>) mMap.values ();
        Iterator<BandData> iter = coll.iterator ();
        return iter;
    }

    public List<BandData> sorted ()
    {
        List data = new ArrayList (mMap.values ());
        Collections.sort (data, new Utils.EventTimeComparator ());
        return data;
    }

    public BandData[] toArray() {
        return mMap.values().toArray(new BandData[0]);
    }
}
