package lt.rtfb.fest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WelcomeFragment extends Fragment
{
    public static final String FRAGMENT_TAG = WelcomeFragment.class.getSimpleName();

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup parent,
                              Bundle savedInstanceState)
    {
        View v = inflater.inflate (R.layout.welcome, parent, false);
        TextView tv = (TextView) v.findViewById(R.id.welcome_text);
        String text = "<p>" + getString(R.string.str1) + "</p>";
        text += "<p>" + getString(R.string.str2) + "</p>";
        text += "<p>" + getString(R.string.str3) + "</p>";
        text += "<p>" + getString(R.string.str_about) + "</p>";
        text += "<p>" + getString(R.string.str_about1) + "</p>";
        text += "<p>" + getString(R.string.str_about2) + "</p>";
        text += "<p>" + getString(R.string.str_about3) + "</p>";
        text += "<p>" + getString(R.string.str_main_terms) + "</p>";
        text += "<p>" + getString(R.string.str_camping_sites) + "</p>";
        text += getString(R.string.str_camping_sites1) + "<br/>";
        text += getString(R.string.str_camping_sites2) + "<br/>";
        text += getString(R.string.str_camping_sites3) + "<br/>";
        text += getString(R.string.str_camping_sites4) + "<br/>";
        text += getString(R.string.str_camping_sites5) + "<br/>";
        text += getString(R.string.str_camping_sites6) + "<br/>";
        text += getString(R.string.str_camping_sites7) + "<br/></p>";
        text += "<p>" + getString(R.string.str_vehicles) + "</p>";
        text += getString(R.string.str_vehicles1) + "<br/>";
        text += getString(R.string.str_vehicles2) + "<br/>";
        text += getString(R.string.str_vehicles3) + "<br/>";
        text += getString(R.string.str_vehicles4) + "<br/></p>";
        text += "<p>" + getString(R.string.str_order) + "</p>";
        text += getString(R.string.str_order1) + "<br/>";
        text += getString(R.string.str_order2) + "<br/>";
        text += getString(R.string.str_order3) + "<br/></p>";
        text += "<p>" + getString(R.string.str_swimming) + "</p>";
        text += getString(R.string.str_swimming1) + "<br/>";
        text += getString(R.string.str_swimming2) + "<br/>";
        text += getString(R.string.str_swimming3) + "<br/></p>";
        text += "<p>" + getString(R.string.str_fires) + "</p>";
        text += getString(R.string.str_fires1) + "<br/>";
        text += getString(R.string.str_fires2) + "<br/></p>";
        text += "<p>" + getString(R.string.str_food_and_drinks) + "</p>";
        text += getString(R.string.str_food_and_drinks1) + "<br/>";
        text += getString(R.string.str_food_and_drinks2) + "<br/>";
        text += getString(R.string.str_food_and_drinks3) + "<br/>";
        text += getString(R.string.str_food_and_drinks4) + "<br/>";
        text += getString(R.string.str_food_and_drinks5) + "<br/></p>";
        text += "<p>" + getString(R.string.str_pets) + "</p>";
        text += getString(R.string.str_pets1) + "<br/>";
        text += getString(R.string.str_pets2) + "<br/></p>";
        text += "<p>" + getString(R.string.str_first_aid) + "</p>";
        text += getString(R.string.str_first_aid1) + "<br/></p>";
        text += "<p>" + getString(R.string.str_water_and_hygiene) + "</p>";
        text += getString(R.string.str_water_and_hygiene1) + "<br/>";
        text += getString(R.string.str_water_and_hygiene2) + "<br/>";
        text += getString(R.string.str_water_and_hygiene3) + "<br/></p>";
        text += "<p>" + getString(R.string.str_cash) + "</p>";
        text += getString(R.string.str_cash1) + "<br/></p>";
        text += "<p>" + getString(R.string.str4) + "</p>";
        text += getString(R.string.str_legend);
        tv.setText(Html.fromHtml(text));

        return v;
    }

    @Override
    public void onResume ()
    {
        super.onResume ();
    }
}
