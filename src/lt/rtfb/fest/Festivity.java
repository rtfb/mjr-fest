package lt.rtfb.fest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

public class Festivity extends FragmentActivity
{
    public static final String APP_STATE_PREFS = "AppState";
    private static final String PREF_ACTIVE_TAB = "ActiveTab";
    public static final String PREF_ACTIVE_TTABLE = "ActiveTimetable";
    public static final String PREF_MAP_ZOOM_LEVEL = "MapZoomLevel";
    public static final String PREF_MAP_CENTER_LAT = "MapCenterLatitude";
    public static final String PREF_MAP_CENTER_LON = "MapCenterLongitude";
    private MjrApplication mApp = null;
    private Toast mDaysLeftToast = null;
    private TabManager mTabManager = null;
    private ServiceConnection mAlarmConnection = new ServiceConnection ()
    {
        public void onServiceConnected (ComponentName className,
                                        IBinder service)
        {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to interact
            // with the service. Because we have bound to a explicit service
            // that we know is running in our own process, we can cast its
            // IBinder to a concrete class and directly access it.
            AlarmService svc
                = ((AlarmService.LocalBinder)service).getService ();
            mApp.setAlarmService(svc);
            log ("Alarm service connected");
            mApp.updateAlarmInfo(null);
        }

        public void onServiceDisconnected (ComponentName className)
        {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never see
            // this happen.
            mApp.setAlarmService (null);
            log ("service disconnected");
        }
    };

    private void startServices ()
    {
        Intent intent = new Intent (this, AlarmService.class);
        intent.putExtra ("message", "initial start from Activity");
        startService (intent);
    }

    void bindServices ()
    {
        // Establish a connection with the service. We use an explicit class
        // name because we want a specific service implementation that we know
        // will be running in our own process (and thus won't be supporting
        // component replacement by other applications).
        bindService (new Intent (this, AlarmService.class),
                     mAlarmConnection, Context.BIND_AUTO_CREATE);
    }

    void unbindServices ()
    {
        if (mApp.getAlarmService () != null)
            unbindService (mAlarmConnection);
    }

    public static void log (String s)
    {
        android.util.Log.d ("Fest", s);
    }

    // Called when the activity is first created.
    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        mApp = (MjrApplication) getApplication ();

        // Workaround a failed download bug. It should not manifest itself in
        // Froyo and up according to this blog post:
        // http://android-developers.blogspot.ca/2011/09/androids-http-clients.html
        // However, I still reproduce it (on Cyanogen). Having no better
        // choice, I apply it unconditionally.
        System.setProperty ("http.keepAlive", "false");

        startServices ();
        bindServices ();

        // On Honeycomb+ we need the title to have ActionBar working
        if (android.os.Build.VERSION.SDK_INT < 11)
            requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView (R.layout.main);

        if (android.os.Build.VERSION.SDK_INT < 11)
            mTabManager = new TabManagerPreHoneycomb(this, R.id.realtabcontent);
        else
            if (Utils.isXlargeScreen(this) || android.os.Build.VERSION.SDK_INT < 14)
                mTabManager = new TabManagerHoneycomb(this, R.id.main_area);
            else
                mTabManager = new TabManagerHoneycombList(this, R.id.main_area);

        mTabManager.init();

        Intent intent = getIntent();
        boolean fromNotification = intent.getStringExtra("band_id") != null;

        if (fromNotification)
            mTabManager.setCurrentTabByTag(mTabManager.getTimetableFragName());
        else {
            SharedPreferences settings = getSharedPreferences(APP_STATE_PREFS, 0);
            String activeTab = settings.getString(PREF_ACTIVE_TAB, "");
            mTabManager.setCurrentTabByTag(activeTab);
        }

        showDaysLeft ();
    }

    public TabManager getTabManager() {
        return mTabManager;
    }

    public MapFragment getMapFragment() {
        FragmentManager fm = getSupportFragmentManager();
        String tag = MapFragment.FRAGMENT_TAG;
        return (MapFragment) fm.findFragmentByTag(tag);
    }

    @Override
    protected void onDestroy ()
    {
        super.onDestroy ();

        if (mDaysLeftToast != null)
            mDaysLeftToast.cancel ();

        unbindServices ();
    }

    @Override
    public void onNewIntent (Intent intent)
    {
        String bandId = intent.getStringExtra ("band_id");

        if (bandId == null)
            return;

        mTabManager.setCurrentTabByTag(mTabManager.getTimetableFragName());
        getTabManager().setTimetableSelection(0); // TODO: turn this int to a constant
    }

    @Override
    protected void onStop ()
    {
        super.onStop ();
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences settings = getSharedPreferences(APP_STATE_PREFS, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREF_ACTIVE_TAB, mTabManager.getCurrentTabTag());
        int activeTmtbl = getTabManager().getTimetableSelection();
        editor.putInt(PREF_ACTIVE_TTABLE, activeTmtbl);

        MapFragment mf = getMapFragment();

        if (mf != null) {
            editor.putInt(PREF_MAP_ZOOM_LEVEL, mf.getZoomLevel());
            editor.putInt(PREF_MAP_CENTER_LAT, mf.getCenterLat());
            editor.putInt(PREF_MAP_CENTER_LON, mf.getCenterLon());
        }

        editor.commit();
    }

    @Override
    protected void onResume ()
    {
        super.onResume ();
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        MenuInflater inflater = getMenuInflater ();
        inflater.inflate (R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId ())
        {
            case R.id.menu_settings:
                Intent i = new Intent (this, SettingsActivity.class);
                startActivityForResult (i, 0);
                return true;
            default:
                return super.onOptionsItemSelected (item);
        }
    }

    private void showDaysLeft ()
    {
        String text = Utils.formatDaysLeft (this);

        if (text == null)
            return;

        int duration = Toast.LENGTH_LONG;
        int gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
        mDaysLeftToast = Toast.makeText (this, text, duration);
        mDaysLeftToast.setGravity (gravity, duration, duration);
        mDaysLeftToast.show ();
    }

    MjrApplication getApp ()
    {
        return (MjrApplication) getApplication ();
    }
}
