package lt.rtfb.fest;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ArrayAdapter;
import java.util.ArrayList;

@TargetApi(14)
public class TabManagerHoneycombList extends TabManager
                                     implements ActionBar.OnNavigationListener {
    private ActionBar mActionBar;
    private int mTabIndex = 0;
    private ArrayList<String> mTabLabels = null;
    private ArrayList<String> mTabTags = null;

    private static final class TabInfo extends TabManager.TabInfo {
        private int position;

        public TabInfo(String tag, Class<?> clss, Bundle bundle,
                       int position) {
            super(tag, clss, bundle);
            this.position = position;
        }
    }

    public TabManagerHoneycombList(FragmentActivity fragmentActivity,
                                   int containerId) {
        super(fragmentActivity, containerId);
        mActionBar = mFragmentActivity.getActionBar();
        mTabLabels = new ArrayList();
        mTabTags = new ArrayList();
    }

    public void init() {
        ActionBar bar = mFragmentActivity.getActionBar();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        bar.setDisplayHomeAsUpEnabled(false);

        addTab(WelcomeFragment.class, null, R.string.welcome_label,
               WelcomeFragment.FRAGMENT_TAG);
        addTab(TimetableFragment.class, null, R.string.timetable_label,
               TimetableFragment.FRAGMENT_TAG);
        addTab(MapFragment.class, null, R.string.map_label,
               MapFragment.FRAGMENT_TAG);

        ArrayAdapter adapter
            = new ArrayAdapter(bar.getThemedContext(),
                               android.R.layout.simple_list_item_1,
                               android.R.id.text1,
                               mTabLabels.toArray());

        bar.setListNavigationCallbacks(adapter, this);
    }

    private void addTab(Class<?> clss, Bundle bundle, int label, String tag) {
        TabInfo tabInfo = new TabInfo (tag, clss, bundle, mTabIndex++);
        detach(tabInfo);
        mTabLabels.add(mFragmentActivity.getString(label));
        mTabTags.add(tag);
        mTabs.put(tag, tabInfo);
    }

    public boolean onNavigationItemSelected(int position, long id) {
        String tag = positionToTag(position);
        switchTabByTag(tag);
        return true;
    }

    private String positionToTag(int position) {
        return mTabTags.get(position);
    }

    public void setCurrentTabByTag(String tag) {
        TabInfo tabInfo = (TabInfo) mTabs.get(tag);

        if (tabInfo != null)
            mActionBar.setSelectedNavigationItem(tabInfo.position);
    }

    public String getCurrentTabTag() {
        return positionToTag(mActionBar.getSelectedNavigationIndex());
    }
}
