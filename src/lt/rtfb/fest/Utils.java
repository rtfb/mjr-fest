package lt.rtfb.fest;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Utils
{
    public static String readFile (AssetManager am, String file)
    {
        try
        {
            InputStream is = am.open (file);
            return readFile (is);
        }
        catch (IOException ioe)
        {
            Festivity.log ("Exception while reading the file " + ioe);
        }

        return "";
    }

    public static String readFile (String uri)
    {
        InputStream in = null;

        try
        {
            File file = new File (new URI (uri));
            in = new FileInputStream (file);
            return readFile (in);
        }
        catch (FileNotFoundException e)
        {
            Festivity.log ("File not found " + e);
        }
        catch (URISyntaxException use)
        {
            Festivity.log ("Malformed URI: " + use);
        }
        finally
        {
            try
            {
                if (in != null)
                    in.close ();
            }
            catch (IOException ioe)
            {
                Festivity.log ("Error while closing the stream :" + ioe);
            }
        }

        return "";
    }

    public static String readFile (InputStream stream)
    {
        BufferedInputStream bis = null;

        try
        {
            bis = new BufferedInputStream (stream, 8 * 1024);
            byte[] contents = new byte[1024];

            int bytesRead = 0;
            String strFileContents = "";

            while ((bytesRead = bis.read (contents)) != -1)
            {
                strFileContents += new String (contents, 0, bytesRead);
            }

            return strFileContents;
        }
        catch (FileNotFoundException e)
        {
            Festivity.log ("File not found " + e);
        }
        catch (IOException ioe)
        {
            Festivity.log ("Exception while reading the file " + ioe);
        }
        finally
        {
            try
            {
                if (bis != null)
                    bis.close ();
            }
            catch (IOException ioe)
            {
                Festivity.log ("Error while closing the stream :" + ioe);
            }
        }

        return "";
    }

    public static String writeFile (File f, byte[] data)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream (f);
            fos.write (data);
            fos.close ();
            return f.toURI ().toString ();
        }
        catch (IOException ioe)
        {
            String m = String.format ("failed to write '%s'", f.getName ());
            Festivity.log (m);
            return null;
        }
    }

    public static class EventTimeComparator implements Comparator<BandData>
    {
        @Override
        public int compare (BandData o1, BandData o2)
        {
            long t1 = o1.getTime ();
            long t2 = o2.getTime ();
            return (t1 < t2 ? -1 : (t1 == t2 ? 0 : 1));
        }
    }

    public static boolean isOnline (Context ctx)
    {
        String conn = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm
            = (ConnectivityManager) ctx.getSystemService (conn);
        NetworkInfo ni = cm.getActiveNetworkInfo ();

        if (ni == null)
            return false;

        return ni.isConnectedOrConnecting ();
    }

    static Calendar FestDay = new GregorianCalendar(2013, 7, 23);
    private static long FridayNoon
        = new GregorianCalendar(2013, 7, 23, 17, 00).getTime().getTime();
    private static long SundayNoon
        = new GregorianCalendar(2013, 7, 25, 17, 00).getTime().getTime();
    public static String formatDaysLeft (Activity a)
    {
        Date now = new Date (System.currentTimeMillis ());

        if (now.getTime() > SundayNoon)
            return a.getString (R.string.days_left_next_year);

        // Don't annoy people with this toast during the fest, return null:
        if (now.getTime() > FridayNoon)
            return null;

        float diffInDays = calculateDaysLeft ();
        int diffInDaysInt = (int)android.util.FloatMath.ceil (diffInDays);

        if (diffInDaysInt > 9)
        {
            String fmt = a.getString (R.string.days_left_ten_plus);
            return String.format (fmt, diffInDaysInt);
        }
        else if (diffInDaysInt > 1 && diffInDaysInt <= 9)
        {
            String fmt = a.getString (R.string.days_left_sub_ten);
            return String.format (fmt, diffInDaysInt);
        }
        else if (diffInDaysInt > 0 && diffInDaysInt <= 1)
            return a.getString (R.string.days_left_tomorrow);
        else
            return a.getString (R.string.days_left_today);
    }

    public static float calculateDaysLeft ()
    {
        Calendar today = Calendar.getInstance ();
        float diff = FestDay.getTimeInMillis() - today.getTimeInMillis();
        return diff / (1000 * 60 * 60 * 24);
    }

    public static void ripoffSystemResource (String resource,
                                             File cacheDir,
                                             String fileName,
                                             DisplayMetrics metrics)
    {
        try
        {
            int id = Resources.getSystem ().getIdentifier (resource, "drawable",
                                                           "android");
            BitmapFactory.Options opts = new BitmapFactory.Options ();
            opts.inDensity = metrics.densityDpi;
            opts.inTargetDensity = 160;
            Bitmap bm = BitmapFactory.decodeResource (Resources.getSystem (),
                                                      id, opts);
            File file = new File (cacheDir, fileName);
            FileOutputStream outStream = new FileOutputStream (file);
            bm.compress (Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush ();
            outStream.close ();
        }
        catch (Exception e)
        {}
    }

    private abstract static class Worker {
        public static boolean READ_ONLY = false;
        public static boolean WRITABLE = true;
        protected SQLiteDatabase mDb = null;
        private boolean mNeedWriteAccess;

        public Worker(boolean needWriteAccess) {
            mNeedWriteAccess = needWriteAccess;
        }

        public void setDb(SQLiteDatabase db) {
            mDb = db;
        }

        public boolean needWriteAccess() {
            return mNeedWriteAccess;
        }

        protected Cursor query(String sql) {
            Cursor cursor = mDb.rawQuery(sql, null);
            cursor.moveToFirst();
            return cursor;
        }

        public abstract BandData[] work();
    }

    private static class DbAccessWrapper {
        private DbHelper mDbHelper = null;
        private Worker mWorker = null;

        public DbAccessWrapper(Context context, Worker w) {
            mDbHelper = new DbHelper(context);
            mWorker = w;
        }

        public BandData[] query() {
            SQLiteDatabase db = null;
            boolean doTransactions = false;

            try {
                if (mWorker.needWriteAccess()) {
                    db = mDbHelper.getWritableDatabase();
                    doTransactions = true;
                }
                else
                    db = mDbHelper.getReadableDatabase();

                if (doTransactions)
                    db.beginTransaction();

                mWorker.setDb(db);
                BandData[] result = mWorker.work();

                if (doTransactions)
                    db.setTransactionSuccessful();

                return result;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            finally {
                if (doTransactions)
                    db.endTransaction();

                try {
                    mDbHelper.close();
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    db.close();
                }
            }

            return null;
        }
    }

    public static BandData[] loadFromDb(Context ctx, final BandData.Kind kind) {
        DbAccessWrapper daw = new DbAccessWrapper(ctx,
            new Worker(Worker.READ_ONLY) {
                public BandData[] work() {
                    Cursor cursor = query(BandData.getSelectSql(kind));
                    int count = cursor.getCount();
                    BandData[] coll = new BandData[count];

                    for (int i = 0; i < count; ++i) {
                        coll[i] = new BandData(cursor);
                        cursor.moveToNext();
                    }

                    return coll;
                }
            });
        return daw.query();
    }

    public static BandData[] loadFromDb(Context ctx, final int status) {
        DbAccessWrapper daw = new DbAccessWrapper(ctx,
            new Worker(Worker.READ_ONLY) {
                public BandData[] work() {
                    Cursor cursor = query(BandData.getSelectSql(status));
                    int count = cursor.getCount();
                    BandData[] coll = new BandData[count];

                    for (int i = 0; i < count; ++i) {
                        coll[i] = new BandData(cursor);
                        cursor.moveToNext();
                    }

                    return coll;
                }
            });
        return daw.query();
    }

    public static BandData loadItem(Context ctx, final String id) {
        DbAccessWrapper daw = new DbAccessWrapper(ctx,
            new Worker(Worker.READ_ONLY) {
                public BandData[] work() {
                    return new BandData[] {
                        new BandData(query(BandData.getSelectSql(id)))
                    };
                }
            });
        return daw.query()[0];
    }

    public static void updateFavoriteItem(Context ctx, final BandData bd) {
        updateFavoriteItem(ctx, bd, bd.getStatus());
    }

    public static void updateFavoriteItem(Context ctx, final BandData bd, final int status) {
        DbAccessWrapper daw = new DbAccessWrapper(ctx,
            new Worker(Worker.WRITABLE) {
                public BandData[] work() {
                    ContentValues values = new ContentValues();
                    values.put("status", status);
                    mDb.update("ttable_entry", values,
                               "id=?", new String[] { bd.getId() });
                    return null;
                }
            });
        daw.query();
    }

    public static Date parseDatetime(String dt) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm",
                                        Locale.US).parse(dt);
        }
        catch (ParseException pe) {
            Festivity.log("ParseException while parsing date " + dt);
            return new Date();
        }
    }

    public static boolean isXlargeScreen(Activity activity) {
        Configuration c = activity.getResources().getConfiguration();
        int screenLayout = c.screenLayout;

        if (((screenLayout & Configuration.SCREENLAYOUT_SIZE_XLARGE)
             == Configuration.SCREENLAYOUT_SIZE_XLARGE))
            return true;

        return false;
    }
}
