package lt.rtfb.fest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DbHelper extends SQLiteOpenHelper
{
    private static final String DB_NAME = "timetable.db";
    private static final String ZIP_NAME = DB_NAME + ".zip";
    private static String DB_PATH = null;
    private final Context mContext;

    public DbHelper (Context context)
    {
        super (context, DB_NAME, null,
               context.getResources ().getInteger (R.integer.db_schema_version));
        mContext = context;
        DB_PATH = mContext.getDatabasePath (DB_NAME).getAbsolutePath ();

        if (!exists(DB_PATH)) {
            try {
                copyDataBase();
            } catch (IOException e) {
                Festivity.log("Error copying database: " + e.getMessage());
            }
        }
    }

    private boolean exists(String path) {
        File f = new File(path);
        return f.exists();
    }

    private void copyDataBase() throws IOException {
        InputStream zip = mContext.getAssets().open(ZIP_NAME);
        Festivity.log("1. zip = " + zip);
        File f = mContext.getDatabasePath(DB_NAME).getParentFile();

        if (!f.exists())
            f.mkdirs();

        ZipInputStream zis = getFileFromZip(zip);

        if (zis == null) {
            throw new IOException("Archive is missing a SQLite database file");
        }

        Festivity.log("2");
        OutputStream output = new FileOutputStream(DB_PATH);

        Festivity.log("3");
        byte[] buffer = new byte[4 * 1024];
        int length;

        while ((length = zis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
        zis.close();
    }

    private ZipInputStream getFileFromZip(InputStream zipFileStream) throws FileNotFoundException, IOException {
        ZipInputStream zis = new ZipInputStream(zipFileStream);
        ZipEntry ze = null;
        while ((ze = zis.getNextEntry()) != null) {
            Festivity.log("extracting file: '" + ze.getName() + "'...");
            return zis;
        }
        return null;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    public static String getLangSelectSql() {
        return getLangSelectSql(java.util.Locale.getDefault().getLanguage());
    }

    public static String getLangSelectSql(String lang) {
        String selectFmt
            = "select l.id from languages as l where l.language='%s'";
        return String.format(selectFmt, lang);
    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion)
    {
        Festivity.log ("DbHelper.onUpgrade");
    }

    @Override
    public void onOpen (SQLiteDatabase db)
    {
        Festivity.log ("DbHelper.onOpen");
        super.onOpen (db);
    }
}
