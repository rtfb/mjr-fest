package lt.rtfb.fest;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class EventDetailsActivity extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            EventDetailsFragment frag = new EventDetailsFragment();
            frag.setArguments(getIntent().getExtras());
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction xaction = manager.beginTransaction();
            xaction.add(android.R.id.content, frag, frag.tagName()).commit();
        }
    }
}
