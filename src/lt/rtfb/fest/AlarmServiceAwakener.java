package lt.rtfb.fest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmServiceAwakener extends BroadcastReceiver
{
    @Override
    public void onReceive (Context context, Intent intent)
    {
        Festivity.log ("AlarmServiceAwakener.onReceive!");
        Festivity.log ("band " + intent.getStringExtra ("band_id"));
        String bandId = intent.getStringExtra ("band_id");
        Intent serviceIntent = new Intent (context, AlarmService.class);
        serviceIntent.putExtra ("message", "alarm wakeup");
        serviceIntent.putExtra ("band_id", bandId);
        context.startService (serviceIntent);
    }
}
