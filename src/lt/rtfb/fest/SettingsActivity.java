package lt.rtfb.fest;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity
{
    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        addPreferencesFromResource (R.xml.preferences);
        Preference button = (Preference) findPreference ("gps_button");
        button.setOnPreferenceClickListener (new Preference.OnPreferenceClickListener ()
        {
            @Override
            public boolean onPreferenceClick (Preference arg0)
            {
                String act = android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                startActivityForResult (new Intent (act), 0);
                return true;
            }
        });
    }
}
