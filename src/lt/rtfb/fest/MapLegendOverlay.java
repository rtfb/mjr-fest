package lt.rtfb.fest;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import java.util.ArrayList;

public class MapLegendOverlay extends ItemizedOverlay<OverlayItem>
{
    private ArrayList<OverlayItem> mOverlays;
    private Context mContext;
    private Toast mToast = null;

    public MapLegendOverlay (Drawable defaultMarker, Context ctx)
    {
        super (boundCenterBottom (defaultMarker));
        mOverlays = new ArrayList<OverlayItem> ();
        mContext = ctx;
        mToast = new Toast (mContext);
        populate ();
    }

    public void addOverlay (OverlayItem overlay, Drawable d)
    {
        overlay.setMarker (boundCenterBottom (d));
        mOverlays.add (overlay);
        populate ();
    }

    @Override
    protected OverlayItem createItem (int i)
    {
        return mOverlays.get (i);
    }

    public void removeItem (int i)
    {
        mOverlays.remove (i);
        populate ();
    }

    @Override
    protected boolean onTap (int i)
    {
        OverlayItem oi = mOverlays.get (i);
        Activity a = (Activity) mContext;
        LayoutInflater inflater = a.getLayoutInflater ();
        ViewGroup vg = (ViewGroup) a.findViewById (R.id.toast_layout_root);
        View layout = inflater.inflate (R.layout.toast_layout, vg);

        ImageView image = (ImageView) layout.findViewById (R.id.image);
        CustomOverlayItem coi = (CustomOverlayItem) oi;
        image.setImageResource (coi.getDrawableId ());
        TextView tv = (TextView) layout.findViewById (R.id.text);
        tv.setText (oi.getTitle ());
        tv = (TextView) layout.findViewById (R.id.text2);
        tv.setText (oi.getSnippet ());

        mToast.setGravity (Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
        mToast.setMargin (0f, 0.12f);
        mToast.setDuration (Toast.LENGTH_LONG);
        mToast.setView (layout);
        mToast.show ();

        return true;
    }

    @Override
    public int size ()
    {
        return mOverlays.size ();
    }

    public void cancelToast ()
    {
        mToast.cancel ();
    }
}
