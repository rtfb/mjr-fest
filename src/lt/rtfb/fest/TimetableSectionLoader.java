package lt.rtfb.fest;

import android.os.AsyncTask;

class TimetableSectionLoader extends AsyncTask<String, Void, String> {
    private TimetableSectionFragment mFragment;
    private BandData[] mItems = null;

    public TimetableSectionLoader(TimetableSectionFragment frag) {
        mFragment = frag;
    }

    @Override
    protected String doInBackground(String... params) {
        mItems = Utils.loadFromDb(mFragment.getActivity(), mFragment.getKind());
        return "DONE";
    }

    @Override
    protected void onPostExecute(String result) {
        mFragment.reload(mItems);
        Festivity.log ("LoadOperation.onPostExecute");
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
