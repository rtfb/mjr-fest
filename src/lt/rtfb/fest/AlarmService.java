package lt.rtfb.fest;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.FloatMath;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AlarmService extends Service
{
    private final int FIFTEEN_MINS_IN_MILLIS = 15 * 60 * 1000;
    private final IBinder mBinder = new LocalBinder ();
    private BandDataCollection<BandData> mCustomItems = null;
    private NotificationManager mNM;
    private final int NOTIF_ID = 1;
    private final boolean DEBUG_LOG = true;

    public class LocalBinder extends Binder
    {
        AlarmService getService ()
        {
            return AlarmService.this;
        }
    }

    @Override
    public void onCreate ()
    {
        log ("AlarmService.onCreate()");
        mNM = (NotificationManager) getSystemService (NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy ()
    {
        log ("AlarmService.onDestroy()");
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId)
    {
        log ("Received start id " + startId + ": " + intent);
        if (intent != null)
            log ("intent.extra = " + intent.getStringExtra ("message"));

        String msg = "";

        if (intent != null)
            msg = intent.getStringExtra ("message");

        if (msg != null && msg.equals ("alarm wakeup"))
        {
            if (mCustomItems == null)
                loadCustomItems();

            String bandId = intent.getStringExtra ("band_id");
            BandData bd = mCustomItems.get (bandId);

            if (bd != null)
            {
                showNotification (bd);
                Utils.updateFavoriteItem(this, bd, BandData.STATUS_ALREADY_NOTIFIED);
                scheduleAlarm (mCustomItems.sorted ());
                mCustomItems.remove(bd.getId());
            }
            else
                log ("dafuq? Got alarm wakeup for band " + bandId
                     + ", but it's not on my list...");
        }

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public IBinder onBind (Intent intent)
    {
        return mBinder;
    }

    private void loadCustomItems ()
    {
        BandData[] items = Utils.loadFromDb(this, BandData.STATUS_CHECKED);
        mCustomItems = new BandDataCollection<BandData>(items);
    }

    public void updateItem(BandData bd) {
        if (mCustomItems == null)
            mCustomItems = new BandDataCollection<BandData>();

        if (bd != null)
            mCustomItems.put(bd);

        scheduleAlarm(mCustomItems.sorted());
    }

    private void scheduleAlarm (List<BandData> items)
    {
        long now = System.currentTimeMillis ();

        Intent alarmIntent = new Intent (this, AlarmServiceAwakener.class);
        int flags = PendingIntent.FLAG_CANCEL_CURRENT;
        PendingIntent cancel = PendingIntent.getBroadcast (this, 0,
                                                           alarmIntent,
                                                           flags);

        // First of all, cancel possibly pending alarms. We don't know yet if
        // the new set of items requires any alarms at all, so start clean.
        getAlarmMgr ().cancel (cancel);

        for (BandData bd : items)
        {
            long bdTime = bd.getTime ();

            if (bdTime < now)
            {
                log (bd.getName () + " is in the past, skip");
                continue;
            }

            alarmIntent.putExtra ("band_id", bd.getId ());
            PendingIntent pending = PendingIntent.getBroadcast (this, 0,
                                                                alarmIntent,
                                                                flags);
            log ("alarm for " + bd.getName () + " @ "
                 + new Date (bdTime - FIFTEEN_MINS_IN_MILLIS).toString ());
            getAlarmMgr ().set (AlarmManager.RTC_WAKEUP,
                                bdTime - FIFTEEN_MINS_IN_MILLIS, pending);
            break;
        }

        log ("no more alarms this time around");
    }

    private AlarmManager getAlarmMgr  ()
    {
        return (AlarmManager) getSystemService (Context.ALARM_SERVICE);
    }

    private SharedPreferences getPrefs ()
    {
        return PreferenceManager.getDefaultSharedPreferences (this);
    }

    private void showNotification (BandData it)
    {
        SharedPreferences prefs = getPrefs ();

        if (!prefs.getBoolean ("notifications_box", false))
            return;

        long currTime = System.currentTimeMillis ();
        long timeLeft = it.getTime () - currTime;
        log ("showing notification, timeLeft = " + timeLeft);
        String ticker = it.getDescr ();

        if (ticker.equals (""))
            ticker = it.getName ();

        int icon = R.drawable.notification_icon;

        if (android.os.Build.VERSION.SDK_INT >= 11)
            icon = R.drawable.icon;

        // Set the icon, scrolling text and timestamp
        Notification notification = new Notification (icon, ticker, currTime);
        notification.defaults = Notification.DEFAULT_SOUND
                                | Notification.DEFAULT_LIGHTS;

        if (prefs.getBoolean ("vibrate_box", false))
            notification.defaults |= Notification.DEFAULT_VIBRATE;

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // The PendingIntent to launch our activity if the user selects this
        // notification
        Intent intent = new Intent (this, Festivity.class);
        intent.putExtra ("band_id", it.getId ());
        PendingIntent contentIntent = PendingIntent.getActivity (this, 0,
                                                                 intent, 0);

        String title = mkTitle (timeLeft, it);
        String message = mkMessage (it);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo (this, title, message, contentIntent);

        // I'm reusing the same notification ID. That means I'm never going to
        // have two notifications, I'll only update the info for the old one.
        // But that's OK for my current purpose: an unread notification about
        // older event will be replaced by info about the newer one.
        mNM.notify (NOTIF_ID, notification);
    }

    private String mkMessage (BandData bd)
    {
        String descr = bd.getDescr ();

        if (descr.equals (""))
            return bd.getName ();
        else
            return descr + " - " + bd.getName ();
    }

    private String mkLocation (BandData.Kind kind)
    {
        switch (kind)
        {
            case BAND_PINE:
                return getString (R.string.venue_pine_stage_loc);
            case BAND_MAIN:
                return getString (R.string.venue_main_stage_loc);
            case BAND_NORD:
                return getString (R.string.venue_nord_stage_loc);
            case LECTURE:
                return getString (R.string.venue_lecture_tent_loc);
            case CRAFT:
                return getString (R.string.venue_crafts_yard_loc);
            default:
                Festivity.log ("ERROR in mkLocation: default branch hit.");
                return "";
        }
    }

    private String mkTitle (long timeLeft, BandData bd)
    {
        float fTimeLeft = timeLeft;
        int minutesLeft = (int) FloatMath.ceil (fTimeLeft / (1000 * 60));

        if (bd.getKind () == BandData.Kind.OTHER
            || bd.getKind () == BandData.Kind.CUSTOM)
            return String.format (getString (R.string.notif_time_left),
                                  minutesLeft);

        String location = mkLocation (bd.getKind ());
        return String.format (getString (R.string.notif_time_left_at),
                              minutesLeft, location);
    }

    // This function is for testing purposes. Should not be called in production.
    @SuppressWarnings("unused")
    private long convertToToday (long currentTimeMillis, long time)
    {
        if (!BuildConfig.DEBUG)
            return time;

        Date showTime = new Date (time);
        Date now = new Date (currentTimeMillis);
        showTime.setYear (now.getYear ());
        showTime.setMonth (now.getMonth ());
        showTime.setDate (now.getDate ());
        return showTime.getTime ();
    }

    // This function is for testing purposes. Should not be called in production.
    @SuppressWarnings("unused")
    private long convertToThisWeek (long currentTimeMillis, long time)
    {
        if (!BuildConfig.DEBUG)
            return time;

        Calendar today = Calendar.getInstance ();
        Calendar showDay = Calendar.getInstance ();
        showDay.setTimeInMillis (time);
        showDay.set (Calendar.WEEK_OF_YEAR, today.get (Calendar.WEEK_OF_YEAR));
        return showDay.getTime ().getTime ();
    }

    @SuppressLint("DefaultLocale")
    private void log (String m)
    {
        if (DEBUG_LOG)
        {
            String ts = String.format ("[%d] ", System.currentTimeMillis ());
            Festivity.log (ts + m);
        }
    }
}
