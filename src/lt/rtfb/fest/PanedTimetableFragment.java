package lt.rtfb.fest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.HashMap;
import java.util.Map;

public class PanedTimetableFragment extends ListFragment {
    public static final String FRAGMENT_TAG = PanedTimetableFragment.class.getSimpleName();
    private MjrApplication mApp = null;
    private int[] mTitles = new int[] {
        R.string.digest_title,
        R.string.pine_stage_title,
        R.string.main_stage_title,
        R.string.northern_stage_title,
        R.string.lectures_title,
        R.string.crafts_title,
        R.string.other_title,
    };
    private BandData.Kind[] mIndexToKind = {
        BandData.Kind.CUSTOM,
        BandData.Kind.BAND_PINE,
        BandData.Kind.BAND_MAIN,
        BandData.Kind.BAND_NORD,
        BandData.Kind.LECTURE,
        BandData.Kind.CRAFT,
        BandData.Kind.OTHER,
    };
    private Map<BandData.Kind, TimetableSectionFragment> mKindToFragment;
    private int mActiveTimetable;

    public PanedTimetableFragment() {
        mKindToFragment = new HashMap();
        mActiveTimetable = -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        mApp = ((Festivity) getActivity()).getApp();
        View v = inflater.inflate(R.layout.timetable, parent, false);
        //FragmentManager fm = getActivity().getSupportFragmentManager();
        //mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), fm);
        //mViewPager = (ViewPager) v.findViewById(R.id.pager);
        //mViewPager.setAdapter(mSectionsPagerAdapter);
        //mViewPager.setOnPageChangeListener(mSectionsPagerAdapter);

        String titles[] = new String[mTitles.length];

        for (int i = 0; i < mTitles.length; ++i)
            titles[i] = getActivity().getString(mTitles[i]);

        int simple_list_item_1 = android.R.layout.simple_list_item_1;
        setListAdapter(new ArrayAdapter<String>(getActivity(),
                                                simple_list_item_1,
                                                titles));

        return v;
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        Intent intent = getActivity().getIntent();
        boolean fromNotification = intent.getStringExtra("band_id") != null;

        if (fromNotification)
            activateTimetable(0);
        else
            setStoredSelection();
    }

    @Override
    public void onResume() {
        super.onResume();

        BandData bd = mApp.getTimetableEntryToClear();

        if (bd == null)
            return;

        //mSectionsPagerAdapter.reloadFavorites(bd);
        //mSectionsPagerAdapter.reload(bd);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        l.setItemChecked(position, true);
        BandData.Kind kind = mIndexToKind[position];
        TimetableSectionFragment fragment = mKindToFragment.get(kind);

        if (fragment == null) {
            fragment = new TimetableSectionFragment();
            Bundle args = new Bundle();
            args.putInt(TimetableSectionFragment.ARG_SECTION_KIND,
                        kind.ordinal());
            fragment.setArguments(args);
            fragment.setThreePaneFrag(this);
            mKindToFragment.put(kind, fragment);
        }

        //addOrReplace(R.id.timetable_section, fragment, "TimetableSectionFragment");
        mActiveTimetable = position;
    }

    void addOrReplace(int container, Fragment fragment, String tag) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction xaction = manager.beginTransaction();
        Fragment existing = manager.findFragmentById(container);

        if (existing == null)
            xaction
                .add(container, fragment, tag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        else
            xaction
                .replace(container, fragment, tag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        xaction.commit();
    }

    public int getSelection() {
        return mActiveTimetable;
    }

    public void setStoredSelection() {
        SharedPreferences settings
            = getActivity().getSharedPreferences(Festivity.APP_STATE_PREFS, 0);
        int activeTimetable = settings.getInt(Festivity.PREF_ACTIVE_TTABLE, 1);
        activateTimetable(activeTimetable);
    }

    public void activateTimetable(int position) {
        if (position < 0)
            return;

        ListView lv = getListView();
        View v = lv.getAdapter().getView(position, null, null);
        long id = lv.getAdapter().getItemId(position);
        lv.performItemClick(v, position, id);
    }

    public void bandClicked(BandData bd) {
        EventDetailsFragment fragment = new EventDetailsFragment();
        Bundle b = new Bundle();
        b.putString("band_id", bd.getId());
        fragment.setArguments(b);
        //addOrReplace(R.id.details_section, fragment, fragment.tagName());
    }

    public void reloadFavorites(BandData bd) {
        TimetableSectionFragment f = mKindToFragment.get(BandData.Kind.CUSTOM);

        if (f != null)
            f.reload(bd);
    }

    public TimetableSectionFragment getCurrFragment() {
        BandData.Kind kind = mIndexToKind[mActiveTimetable];
        TimetableSectionFragment fragment = mKindToFragment.get(kind);
        return fragment;
    }
}
