package lt.rtfb.fest;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;

public class TabManagerPreHoneycomb extends TabManager
                                    implements TabHost.OnTabChangeListener {
    private TabHost mTabHost = null;

    private static class DummyTabContentFactory implements TabContentFactory {
        private final Context mContext;

        public DummyTabContentFactory(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View view = new View(mContext);
            view.setMinimumWidth(0);
            view.setMinimumHeight(0);
            return view;
        }
    }

    public TabManagerPreHoneycomb(FragmentActivity fragmentActivity,
                                  int containerId) {
        super(fragmentActivity, containerId);
        mTabHost = (TabHost) mFragmentActivity.findViewById(android.R.id.tabhost);
        mTabHost.setup();
        mTabHost.setOnTabChangedListener(this);
    }

    public void init() {
        addTab(WelcomeFragment.class, null, R.drawable.ic_tab_welcome,
               R.string.welcome_label);
        addTab(TimetableFragment.class, null, R.drawable.ic_tab_programme,
               R.string.timetable_label);
        addTab(MapFragment.class, null, R.drawable.ic_tab_map,
               R.string.map_label);
    }

    private void addTab(Class<?> clss, Bundle bundle, int drawable, int label) {
        TabSpec tabSpec = mkTabSpec(clss.getSimpleName(), drawable, label);
        tabSpec.setContent (new DummyTabContentFactory (mFragmentActivity));

        String tag = tabSpec.getTag ();
        TabInfo tabInfo = new TabInfo (tag, clss, bundle);
        detach(tabInfo);
        mTabs.put (tag, tabInfo);
        mTabHost.addTab (tabSpec);
    }

    private TabHost.TabSpec mkTabSpec(String tag, int drawable, int labelRsrc) {
        TabHost.TabSpec spec = mTabHost.newTabSpec(tag);
        Drawable d = mFragmentActivity.getResources().getDrawable(drawable);
        String label = mFragmentActivity.getString(labelRsrc);
        spec.setIndicator(label, d);
        return spec;
    }

    @Override
    public void onTabChanged(String tag) {
        switchTabByTag(tag);
    }

    public String getCurrentTabTag() {
        return mTabHost.getCurrentTabTag();
    }

    public void setCurrentTabByTag(String tag) {
        mTabHost.setCurrentTabByTag(tag);
    }
}
