package lt.rtfb.fest;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import java.util.List;

public class MapFragment extends Fragment
                         implements LocationListener,
                                    SensorEventListener
{
    public static final String FRAGMENT_TAG = MapFragment.class.getSimpleName();

    private ViewGroup mMapViewContainer;
    private LocationManager mLocMgr = null;
    private MapLocationOverlay mLocationOverlay = null;
    private SensorManager mSensorManager;
    private MapView mMapView = null;
    private ImageButton mMyLocationButton = null;
    private long mLastLocationMillis;
    private GpsStatusListener mGpsStatusListener = new GpsStatusListener ();
    private OverlayItemData[] mOverlayItems =
    {
        new OverlayItemData (55735725, 26238785,
                             R.string.overlay_name_gate,
                             R.string.overlay_msg_gate,
                             R.drawable.org),
        new OverlayItemData (55734082, 26225438,
                             R.string.overlay_name_vip_parking,
                             R.string.overlay_msg_vip_parking,
                             R.drawable.vip),
        new OverlayItemData (55733345, 26227434,
                             R.string.overlay_name_beer3,
                             R.string.overlay_msg_beer3,
                             R.drawable.beer),
        new OverlayItemData (55734384, 26228292,
                             R.string.overlay_name_food2,
                             R.string.overlay_msg_food2,
                             R.drawable.food),
        new OverlayItemData (55734432, 26229300,
                             R.string.overlay_name_food4,
                             R.string.overlay_msg_food4,
                             R.drawable.coffee),
        new OverlayItemData (55733852, 26230223,
                             R.string.overlay_name_aerodrome,
                             R.string.overlay_msg_aerodrome,
                             R.drawable.airport),
        new OverlayItemData (55732393, 26229440,
                             R.string.overlay_name_naturalists,
                             R.string.overlay_msg_naturalists,
                             R.drawable.ornitologists),
        new OverlayItemData (55731291, 26231022,
                             R.string.overlay_name_food3,
                             R.string.overlay_msg_food3,
                             R.drawable.food),
        new OverlayItemData (55735652, 26235931,
                             R.string.overlay_name_tickets,
                             R.string.overlay_msg_tickets,
                             R.drawable.tickets),
        new OverlayItemData (55735737, 26231554,
                             R.string.overlay_name_security,
                             R.string.overlay_msg_security,
                             R.drawable.security),
        new OverlayItemData (55734688, 26234451,
                             R.string.overlay_name_beach,
                             R.string.overlay_msg_beach1,
                             R.drawable.beach),
        new OverlayItemData (55735435, 26234976,
                             R.string.overlay_name_sports,
                             R.string.overlay_msg_sports1,
                             R.drawable.sports),
        new OverlayItemData (55736063, 26232777,
                             R.string.overlay_name_parking,
                             R.string.overlay_msg_parking,
                             R.drawable.parking),
        new OverlayItemData (55735495, 26228678,
                             R.string.overlay_name_main_camping,
                             R.string.overlay_msg_main_camping,
                             R.drawable.camping),
        new OverlayItemData (55736884, 26227884,
                             R.string.overlay_name_oak,
                             R.string.overlay_msg_oak,
                             R.drawable.oak),
        new OverlayItemData (55736643, 26224730,
                             R.string.overlay_name_lectures_tent,
                             R.string.overlay_msg_lectures_tent,
                             R.drawable.lecturetent),
        new OverlayItemData (55734637, 26229322,
                             R.string.overlay_name_info,
                             R.string.overlay_msg_info,
                             R.drawable.info),
        new OverlayItemData (55734100, 26227930,
                             R.string.overlay_name_main_stage,
                             R.string.overlay_msg_main_stage,
                             R.drawable.mainstage),
        new OverlayItemData (55733937, 26229064,
                             R.string.overlay_name_beer1,
                             R.string.overlay_msg_beer1,
                             R.drawable.beer),
        new OverlayItemData (55733683, 26228721,
                             R.string.overlay_name_food1,
                             R.string.overlay_msg_food,
                             R.drawable.food),
        new OverlayItemData (55733669, 26227140,
                             R.string.overlay_name_first_aid,
                             R.string.overlay_msg_first_aid,
                             R.drawable.firstaid),
        new OverlayItemData (55733827, 26227521,
                             R.string.overlay_name_org,
                             R.string.overlay_msg_org,
                             R.drawable.org),
        new OverlayItemData (55733200, 26226983,
                             R.string.overlay_name_beach,
                             R.string.overlay_msg_beach2,
                             R.drawable.beach),
        new OverlayItemData (55734130, 26224215,
                             R.string.overlay_name_vip_camping,
                             R.string.overlay_msg_vip_camping,
                             R.drawable.vip),
        new OverlayItemData (55732620, 26230245,
                             R.string.overlay_name_crafts_yard,
                             R.string.overlay_msg_crafts_yard,
                             R.drawable.crafts),
        new OverlayItemData (55731850, 26230963,
                             R.string.overlay_name_pinewood_stage,
                             R.string.overlay_msg_pinewood_stage,
                             R.drawable.pinestage),
        new OverlayItemData (55731339, 26230631,
                             R.string.overlay_name_beer2,
                             R.string.overlay_msg_beer2,
                             R.drawable.beer),
        new OverlayItemData (55735302, 26223807,
                             R.string.overlay_name_ripka,
                             R.string.overlay_msg_sports2,
                             R.drawable.ripka),
        new OverlayItemData (55734879, 26223625,
                             R.string.overlay_name_family_camping,
                             R.string.overlay_msg_family_camping,
                             R.drawable.familycamping),
        new OverlayItemData (55736715, 26222949,
                             R.string.overlay_name_northern_stage,
                             R.string.overlay_msg_northern_stage,
                             R.drawable.northstage),
    };
    private MapLegendOverlay mLegend = null;

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState)
    {
        mMapViewContainer = new RelativeLayout (getActivity ());
        return mMapViewContainer;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState)
    {
        super.onActivityCreated (savedInstanceState);

        if (mMapView == null)
            initViews ();

        mMapViewContainer.addView (mMapView);

        int wc = LayoutParams.WRAP_CONTENT;
        LayoutParams lp = new LayoutParams (wc, wc);
        mMapViewContainer.addView (mMyLocationButton, lp);
        adjustButtonLayout (mMyLocationButton);
    }

    private void adjustButtonLayout (ImageButton btn)
    {
        LayoutParams params = (LayoutParams) btn.getLayoutParams ();
        params.addRule (RelativeLayout.ALIGN_PARENT_RIGHT);
        params.setMargins (0, 8, 8, 0);
        btn.setLayoutParams (params);
    }

    private void initViews ()
    {
        mMapView = new MapView (getActivity (), BuildValues.MAPS_KEY);
        mMapView.setBuiltInZoomControls (true);
        mMapView.setSatellite (true);
        mMapView.setClickable (true);

        mLocationOverlay = new MapLocationOverlay (getActivity ());
        List<Overlay> overlays = mMapView.getOverlays ();
        overlays.clear ();
        overlays.add (initLegend ());
        overlays.add (mLocationOverlay);
        mMapView.invalidate ();

        int maxZoom = mMapView.getMaxZoomLevel ();
        int initZoom = maxZoom - 4;
        SharedPreferences settings
            = getActivity().getSharedPreferences(Festivity.APP_STATE_PREFS, 0);
        int zoom = settings.getInt(Festivity.PREF_MAP_ZOOM_LEVEL, initZoom);
        MapController mapControl = mMapView.getController ();
        mapControl.setZoom(zoom);
        int latitude = settings.getInt(Festivity.PREF_MAP_CENTER_LAT, 55735652);
        int longitude = settings.getInt(Festivity.PREF_MAP_CENTER_LON, 26235931);
        mapControl.animateTo(new GeoPoint(latitude, longitude));

        mMyLocationButton = new ImageButton (getActivity ());
        mMyLocationButton.setBackgroundResource (R.drawable.my_location);
        int id = R.string.icon_my_location;
        String descr = getActivity ().getResources ().getString (id);
        mMyLocationButton.setContentDescription (descr);
        mMyLocationButton.setVisibility (View.INVISIBLE);
        mMyLocationButton.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v)
            {
                animateToLastLocation ();
            }
        });

        registerHeadingListener ();
    }

    @Override
    public void onResume ()
    {
        super.onResume ();
        registerLocationListener ();
    }

    @Override
    public void onPause ()
    {
        super.onPause ();
        mLegend.cancelToast ();
        mLocMgr.removeUpdates (this);
        mLocMgr.removeGpsStatusListener (mGpsStatusListener);
    }

    public int getZoomLevel() {
        return mMapView.getZoomLevel();
    }

    public int getCenterLat() {
        return mMapView.getMapCenter().getLatitudeE6();
    }

    public int getCenterLon() {
        return mMapView.getMapCenter().getLongitudeE6();
    }

    MapLegendOverlay initLegend ()
    {
        Drawable d = getResources ().getDrawable (R.drawable.tickets);
        mLegend = new MapLegendOverlay (d, getActivity ());

        for (int i = 0; i < mOverlayItems.length; i++)
        {
            OverlayItemData oid = mOverlayItems[i];
            d = getResources ().getDrawable (oid.drawable);
            GeoPoint gp = new GeoPoint (oid.latitude, oid.longitude);
            String title = getString (oid.title);
            String info = getString (oid.info);
            CustomOverlayItem coi = new CustomOverlayItem (gp, title, info,
                                                           oid.drawable);
            mLegend.addOverlay (coi, d);
        }

        getApplication ().setMapLegend (mLegend);

        return mLegend;
    }

    @Override
    public void onDestroyView ()
    {
        super.onDestroyView ();
        ViewGroup vg = (ViewGroup) mMapView.getParent ();
        vg.removeView (mMapView);
        vg.removeView (mMyLocationButton);
        getApplication ().setMapLegend (null);
    }

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater menuInflator)
    {
        //menuInflator.inflate (R.menu.map, menu);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu)
    {
        super.onPrepareOptionsMenu (menu);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem menuItem)
    {
        return super.onOptionsItemSelected (menuItem);
    }

    @Override
    public void onLocationChanged (Location location)
    {
        if (mLocationOverlay == null || mMapView == null)
            return;

        mLocationOverlay.setMyLocation (location);

        if (location != null)
            mLastLocationMillis = SystemClock.elapsedRealtime();

        mMapView.postInvalidate ();
    }

    @Override
    public void onProviderEnabled (String provider)
    {
        Festivity.log ("MapFragment.onProviderEnabled: " + provider);
    }

    @Override
    public void onProviderDisabled (String provider)
    {
        Festivity.log ("MapFragment.onProviderDisabled: " + provider);
    }

    @Override
    public void onStatusChanged (String provider, int status, Bundle extras)
    {
        Festivity.log ("MapFragment.onStatusChanged, status = " + status);
    }

    @Override
    public void onAccuracyChanged (Sensor sensor, int accuracy)
    {}

    @Override
    public void onSensorChanged (SensorEvent event)
    {
        float heading = event.values[0];

        if (mLocationOverlay.setHeading (heading))
            mMapView.postInvalidate ();
    }

    private void registerHeadingListener ()
    {
        String svc = Context.SENSOR_SERVICE;
        mSensorManager = (SensorManager) getActivity ().getSystemService (svc);
        int orientationSvc = Sensor.TYPE_ORIENTATION;
        Sensor heading = mSensorManager.getDefaultSensor (orientationSvc);

        if (heading != null)
        {
            int rate = SensorManager.SENSOR_DELAY_UI;
            mSensorManager.registerListener (this, heading, rate);
        }
        else
            Festivity.log ("No heading sensor.");
    }

    private void registerLocationListener ()
    {
        String svc = Context.LOCATION_SERVICE;
        mLocMgr = (LocationManager) getActivity ().getSystemService (svc);
        List<String> providers = mLocMgr.getProviders (true);
        Festivity.log ("Enabled providers = " + providers.toString ());
        String provider = mLocMgr.getBestProvider (new Criteria (), true);
        Festivity.log ("Best provider = " + provider);

        // TODO: drop frequency when MapView is not displayed to the user.
        // Frequent polling kills battery.
        int freq = 1000; // msecs
        float minDistance = 0; // meters

        if (provider != null)
        {
            mLocMgr.requestLocationUpdates (provider, freq, minDistance, this);
            mLocMgr.addGpsStatusListener (mGpsStatusListener);
        }
    }

    private void animateToLastLocation ()
    {
        Location loc = mLocationOverlay.getLastLocation ();

        if (loc == null)
            return;

        GeoPoint gp = MapLocationOverlay.locationToGeoPoint (loc);
        MapController mapController = mMapView.getController ();
        mapController.animateTo (gp);
    }

    MjrApplication getApplication ()
    {
        return (MjrApplication) getActivity ().getApplication ();
    }

    private class GpsStatusListener implements GpsStatus.Listener
    {
        public void onGpsStatusChanged (int event)
        {
            boolean haveGpsFix = false;

            switch (event)
            {
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    if (mLocationOverlay.getLastLocation () != null)
                        haveGpsFix = (SystemClock.elapsedRealtime ()
                                      - mLastLocationMillis) < 3000;

                    Festivity.log ("GPS_EVENT_SATELLITE_STATUS, haveGpsFix: "
                                   + haveGpsFix);
                    break;

                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    Festivity.log ("GPS_EVENT_FIRST_FIX");
                    haveGpsFix = true;
                    break;
            }

            if (haveGpsFix)
                mMyLocationButton.setVisibility (View.VISIBLE);
            else
                mMyLocationButton.setVisibility (View.INVISIBLE);
        }
    }
}

class OverlayItemData
{
    public OverlayItemData (int lat, int lon, int t, int i, int d)
    {
        latitude = lat;
        longitude = lon;
        title = t;
        info = i;
        drawable = d;
    }

    public int longitude;
    public int latitude;
    public int title;
    public int info;
    public int drawable;
}
