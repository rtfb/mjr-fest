package lt.rtfb.fest;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

@TargetApi(11)
public class TabManagerHoneycomb extends TabManager
                                 implements ActionBar.TabListener {
    private ActionBar mActionBar;
    private boolean mThreePaneView = false;

    private static final class TabInfo extends TabManager.TabInfo {
        private ActionBar.Tab tab;

        public TabInfo(String tag, Class<?> clss, Bundle bundle,
                       ActionBar.Tab tab) {
            super(tag, clss, bundle);
            this.tab = tab;
        }
    }

    public TabManagerHoneycomb(FragmentActivity fragmentActivity,
                               int containerId) {
        super(fragmentActivity, containerId);
        mActionBar = mFragmentActivity.getActionBar();
    }

    public void init() {
        ActionBar bar = mFragmentActivity.getActionBar();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        bar.setDisplayHomeAsUpEnabled(false);

        addTab(WelcomeFragment.class, null, R.string.welcome_label,
               WelcomeFragment.FRAGMENT_TAG);
        //mThreePaneView = Utils.isXlargeScreen(mFragmentActivity);
        if (mThreePaneView)
            addTab(PanedTimetableFragment.class, null, R.string.timetable_label,
                   PanedTimetableFragment.FRAGMENT_TAG);
        else
            addTab(TimetableFragment.class, null, R.string.timetable_label,
                   TimetableFragment.FRAGMENT_TAG);
        addTab(MapFragment.class, null, R.string.map_label,
               MapFragment.FRAGMENT_TAG);
    }

    private void addTab(Class<?> clss, Bundle bundle, int label, String tag) {
        ActionBar.Tab tab = mActionBar.newTab();
        TabInfo tabInfo = new TabInfo(tag, clss, bundle, tab);
        detach(tabInfo);
        mTabs.put(tag, tabInfo);
        tab.setText(mFragmentActivity.getString(label));
        tab.setTabListener(this);
        tab.setTag(tag);
        mActionBar.addTab(tab);
    }

    public void onTabSelected(ActionBar.Tab tab,
                              android.app.FragmentTransaction unused) {
        String tag = (String) tab.getTag();
        TabInfo newTabInfo = (TabInfo) mTabs.get(tag);
        FragmentManager fragMgr = mFragmentActivity.getSupportFragmentManager();
        FragmentTransaction xaction = fragMgr.beginTransaction();

        createOrShow(xaction, newTabInfo);

        xaction.commit();
        fragMgr.executePendingTransactions();
    }

    public void onTabUnselected(ActionBar.Tab tab,
                                android.app.FragmentTransaction unused) {
        String tag = (String) tab.getTag();
        TabInfo tabInfo = (TabInfo) mTabs.get(tag);
        FragmentManager fragMgr = mFragmentActivity.getSupportFragmentManager();
        FragmentTransaction xaction = fragMgr.beginTransaction();

        if (tabInfo.fragment == null)
            return;

        xaction.hide(tabInfo.fragment);
        xaction.commit();
        cancelMapToast(tag);
    }

    public void onTabReselected(ActionBar.Tab tab,
                                android.app.FragmentTransaction xaction) {
        // NO-OP
    }

    public void setCurrentTabByTag(String tag) {
        TabInfo tabInfo = (TabInfo) mTabs.get(tag);

        if (tabInfo != null)
            mActionBar.selectTab(tabInfo.tab);
    }

    public String getCurrentTabTag() {
        return (String) mActionBar.getSelectedTab().getTag();
    }

    private PanedTimetableFragment getTimetable() {
        FragmentManager fm = mFragmentActivity.getSupportFragmentManager();
        String tag = PanedTimetableFragment.FRAGMENT_TAG;
        return (PanedTimetableFragment) fm.findFragmentByTag(tag);
    }

    public int getTimetableSelection() {
        if (mThreePaneView) {
            PanedTimetableFragment tf = getTimetable();
            if (tf != null)
                return tf.getSelection();
            else
                return -1;
        } else {
            return super.getTimetableSelection();
        }
    }

    public void setTimetableSelection(int sel) {
        if (mThreePaneView) {
            PanedTimetableFragment tf = getTimetable();
            if (tf != null)
                tf.activateTimetable(sel);
        } else {
            super.setTimetableSelection(sel);
        }
    }

    public String getTimetableFragName() {
        if (mThreePaneView)
            return PanedTimetableFragment.class.getSimpleName();
        else
            return super.getTimetableFragName();
    }

    public TimetableSectionFragment getCurrTimetableSectionFrag() {
        if (mThreePaneView) {
            PanedTimetableFragment tf = getTimetable();
            return tf.getCurrFragment();
        } else {
            return super.getCurrTimetableSectionFrag();
        }
    }
}
