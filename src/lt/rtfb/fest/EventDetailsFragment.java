package lt.rtfb.fest;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.InputStream;
import java.io.IOException;

public class EventDetailsFragment extends Fragment {
    private BandData mBandData;
    OnCheckedChangeListener mCheckListener = null;
    MjrApplication mApp = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.band_info, container, false);

        ImageView ivBandPic = (ImageView) v.findViewById(R.id.band_picture);
        TextView tvShowTime = (TextView) v.findViewById(R.id.band_show_time);
        TextView tvBandName = (TextView) v.findViewById(R.id.band_name);
        TextView tvBandText = (TextView) v.findViewById(R.id.band_text);
        TextView tvStage = (TextView) v.findViewById(R.id.stage_name);
        CheckBox btnLetsgo = (CheckBox) v.findViewById(R.id.letsgo_button);

        mApp = (MjrApplication) getActivity().getApplication();
        Bundle extras = getArguments();
        String id = null;

        if (extras != null)
            id = extras.getString("band_id");

        if (id == null || id.equals("")) {
            Festivity.log("dafuq? BandInfo called without band_id");
            // TODO: do something to avoid null deref below
        }
        else {
            mBandData = Utils.loadItem(getActivity(), id);
        }

        tvBandName.setText(mBandData.getNameForDetails());
        tvBandText.setText(Html.fromHtml(mBandData.getTextForDetails()));

        try {
            ivBandPic.setImageBitmap(loadFromAssets(mBandData.getImage()));
        } catch (IOException ioe) {
            Festivity.log("IOException: " + ioe);
        }

        tvStage.setText(getStageName(mBandData.getKind()));
        int stageResId = mBandData.getMarkerResId();
        tvStage.setCompoundDrawablesWithIntrinsicBounds(0, 0, stageResId, 0);

        mCheckListener = new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                checkItem(isChecked);
            }
        };

        btnLetsgo.setChecked(mBandData.isChecked());
        btnLetsgo.setOnCheckedChangeListener(mCheckListener);

        String day = getString(mBandData.day());
        String time = mBandData.getTimeStr();
        tvShowTime.setText(day + ", " + time);

        return v;
    }

    void checkItem(boolean isChecked) {
        mBandData.setIsChecked(isChecked);
        Utils.updateFavoriteItem(getActivity(), mBandData);
        mApp.updateAlarmInfo(mBandData);
        mApp.setTimetableEntryToClear(mBandData);
    }

    Bitmap loadFromAssets(String name) throws IOException {
        AssetManager assetManager = getActivity().getAssets();
        InputStream istr = assetManager.open(name);
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        istr.close();
        return bitmap;
    }

    String getStageName(BandData.Kind kind) {
        switch (kind) {
            case BAND_PINE:
                return getString(R.string.overlay_name_pinewood_stage);
            case BAND_MAIN:
                return getString(R.string.overlay_name_main_stage);
            case BAND_NORD:
                return getString(R.string.overlay_name_northern_stage);
            case LECTURE:
                return getString(R.string.overlay_name_lectures_tent);
            case CRAFT:
                return getString(R.string.overlay_name_crafts_yard);
            default:
                return getString(R.string.overlay_name_pinewood_stage);
        }
    }

    public static String tagName() {
        return EventDetailsFragment.class.getSimpleName();
    }
}
