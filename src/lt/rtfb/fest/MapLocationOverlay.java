package lt.rtfb.fest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MapLocationOverlay extends Overlay
{
    private final Drawable[] mArrows;
    private int mHeadingIndex = 0;
    private Location mLocation;
    private final int mArrowWidth;
    private final int mArrowHeight;
    private final Paint mErrorCirclePaint;

    public MapLocationOverlay (Context context)
    {
        final Resources resources = context.getResources ();
        mArrows = new Drawable[]
        {
            resources.getDrawable(R.drawable.arrow_0), resources.getDrawable(R.drawable.arrow_20),
            resources.getDrawable(R.drawable.arrow_40), resources.getDrawable(R.drawable.arrow_60),
            resources.getDrawable(R.drawable.arrow_80), resources.getDrawable(R.drawable.arrow_100),
            resources.getDrawable(R.drawable.arrow_120), resources.getDrawable(R.drawable.arrow_140),
            resources.getDrawable(R.drawable.arrow_160), resources.getDrawable(R.drawable.arrow_180),
            resources.getDrawable(R.drawable.arrow_200), resources.getDrawable(R.drawable.arrow_220),
            resources.getDrawable(R.drawable.arrow_240), resources.getDrawable(R.drawable.arrow_260),
            resources.getDrawable(R.drawable.arrow_280), resources.getDrawable(R.drawable.arrow_300),
            resources.getDrawable(R.drawable.arrow_320), resources.getDrawable(R.drawable.arrow_340)
        };

        mArrowWidth = mArrows[mHeadingIndex].getIntrinsicWidth ();
        mArrowHeight = mArrows[mHeadingIndex].getIntrinsicHeight ();

        for (Drawable arrow : mArrows)
            arrow.setBounds (0, 0, mArrowWidth, mArrowHeight);

        mErrorCirclePaint = getPaint (context, R.color.blue);
        mErrorCirclePaint.setAlpha (127);
    }

    public void setMyLocation (Location location)
    {
        mLocation = location;
    }

    public Location getLastLocation ()
    {
        return mLocation;
    }

    public boolean setHeading (float heading)
    {
        // Use -heading because the arrow images are counter-clockwise rather
        // than clockwise.
        final int numArrows = mArrows.length;
        int index = Math.round (-heading / 360 * numArrows);

        while (index < 0)
            index += numArrows;

        while (index > numArrows - 1)
            index -= numArrows;

        if (index != mHeadingIndex)
        {
            mHeadingIndex = index;
            return true;
        }
        else
            return false;
    }

    public static Paint getPaint (Context context, int colorId)
    {
        Paint paint = new Paint ();
        paint.setColor (context.getResources ().getColor (colorId));
        paint.setStrokeWidth (3);
        paint.setStyle (Paint.Style.STROKE);
        paint.setAntiAlias (true);
        return paint;
    }

    public static GeoPoint locationToGeoPoint (Location location)
    {
        return new GeoPoint ((int) (location.getLatitude () * 1E6),
                             (int) (location.getLongitude () * 1E6));
    }

    private void drawMyLocation (Canvas canvas, Projection projection)
    {
        if (mLocation == null)
            return;

        Point point = drawElement (canvas, projection,
                                   locationToGeoPoint (mLocation),
                                   mArrows[mHeadingIndex],
                                   -(mArrowWidth / 2), -(mArrowHeight / 2));

        // Draw the error circle
        float accuracy = mLocation.getAccuracy ();
        float radius = projection.metersToEquatorPixels (accuracy);
        canvas.drawCircle (point.x, point.y, radius, mErrorCirclePaint);
    }

    private Point drawElement (Canvas canvas, Projection projection,
                               GeoPoint geoPoint, Drawable drawable,
                               int offsetX, int offsetY)
    {
        Point point = new Point ();
        projection.toPixels (geoPoint, point);
        canvas.save ();
        canvas.translate (point.x + offsetX, point.y + offsetY);
        drawable.draw (canvas);
        canvas.restore ();
        return point;
    }

    @Override
    public void draw (Canvas canvas, MapView mapView, boolean shadow)
    {
        if (shadow)
            return;

        // It's safe to keep projection within a single draw operation
        Projection projection = mapView.getProjection ();

        if (projection == null)
        {
            Festivity.log ("No projection, unable to draw.");
            return;
        }

        drawMyLocation (canvas, projection);
    }
}
