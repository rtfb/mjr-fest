package lt.rtfb.fest;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import java.util.HashMap;
import java.util.Map;
import junit.framework.Assert;

public class SectionsPagerAdapter extends FragmentPagerAdapter
                                  implements ViewPager.OnPageChangeListener {
    private Activity mActivity = null;
    private int[] mTitles = new int[] {
        R.string.digest_title,
        R.string.pine_stage_title,
        R.string.main_stage_title,
        R.string.northern_stage_title,
        R.string.lectures_title,
        R.string.crafts_title,
        R.string.other_title,
    };
    private BandData.Kind[] mIndexToKind = {
        BandData.Kind.CUSTOM,
        BandData.Kind.BAND_PINE,
        BandData.Kind.BAND_MAIN,
        BandData.Kind.BAND_NORD,
        BandData.Kind.LECTURE,
        BandData.Kind.CRAFT,
        BandData.Kind.OTHER,
    };
    private Map<BandData.Kind, TimetableSectionFragment> mKindToFragment;

    public SectionsPagerAdapter(Activity activity, FragmentManager fm) {
        super(fm);
        Assert.assertEquals(mTitles.length, mIndexToKind.length);
        mActivity = activity;
        mKindToFragment = new HashMap();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = mKindToFragment.get(mIndexToKind[position]);

        if (f != null)
            return f;

        TimetableSectionFragment fragment = new TimetableSectionFragment();
        Bundle args = new Bundle();
        args.putInt(TimetableSectionFragment.ARG_SECTION_KIND,
                    mIndexToKind[position].ordinal());
        fragment.setArguments(args);
        fragment.setAdapter(this);
        mKindToFragment.put(mIndexToKind[position], fragment);
        return fragment;
    }

    public void reload(BandData bd) {
        BandData.Kind kind = bd.getKind();
        Assert.assertFalse(kind == BandData.Kind.CUSTOM);
        TimetableSectionFragment f = mKindToFragment.get(kind);

        if (f != null)
            f.reload(bd);
    }

    public void reloadFavorites(BandData bd) {
        TimetableSectionFragment f = mKindToFragment.get(BandData.Kind.CUSTOM);

        if (f != null)
            f.reload(bd);
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mActivity.getString(mTitles[position]);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels){
    }

    private MjrApplication getApp() {
        return ((Festivity) mActivity).getApp();
    }

    @Override
    public void onPageSelected(int position) {
        BandData bd = getApp().getTimetableEntryToClear();

        if (bd != null) {
            BandData.Kind kind = bd.getKind();
            Assert.assertFalse(kind == BandData.Kind.CUSTOM);
            TimetableSectionFragment f = mKindToFragment.get(kind);

            if (f != null)
                f.getListView().invalidateViews();
        }
    }
}
