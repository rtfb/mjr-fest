package lt.rtfb.fest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TimetableFragment extends Fragment
{
    public static final String FRAGMENT_TAG = TimetableFragment.class.getSimpleName();
    private MjrApplication mApp = null;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup parent,
                              Bundle savedInstanceState)
    {
        mApp = ((Festivity) getActivity ()).getApp ();
        View v = inflater.inflate (R.layout.timetable, parent, false);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), fm);
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(mSectionsPagerAdapter);

        Intent intent = getActivity ().getIntent ();
        boolean fromNotification = intent.getStringExtra ("band_id") != null;

        if (fromNotification)
            setSelection(0);
        else
            setStoredSelection();

        return v;
    }

    @Override
    public void onResume ()
    {
        super.onResume ();

        BandData bd = mApp.getTimetableEntryToClear();

        if (bd == null)
            return;

        mSectionsPagerAdapter.reloadFavorites(bd);
        mSectionsPagerAdapter.reload(bd);
    }

    public void setSelection (int position)
    {
        mViewPager.setCurrentItem(position);
    }

    public int getSelection() {
        return mViewPager.getCurrentItem();
    }

    public void setStoredSelection() {
        SharedPreferences settings
            = getActivity().getSharedPreferences(Festivity.APP_STATE_PREFS, 0);
        int activeTimetable = settings.getInt(Festivity.PREF_ACTIVE_TTABLE, 1);
        mViewPager.setCurrentItem(activeTimetable);
    }

    public TimetableSectionFragment getCurrFragment() {
        return (TimetableSectionFragment) mSectionsPagerAdapter.getItem(getSelection());
    }
}
