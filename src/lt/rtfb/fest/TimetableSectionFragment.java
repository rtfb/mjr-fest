package lt.rtfb.fest;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import junit.framework.Assert;

public class TimetableSectionFragment extends Fragment
                                      implements AdapterView.OnItemClickListener {
    public static final String ARG_SECTION_KIND = "section_kind";
    private ListView mListView = null;
    private View mView = null;
    private SectionsPagerAdapter mAdapter = null;
    private PanedTimetableFragment mThreePaneFrag = null;
    private BandData.Kind mKind;
    private BandDataCollection<BandData> mData = null;
    private static Pair<Integer, Long>[] mFestivalDays = new Pair[] {
        new Pair<Integer, Long>(-1, BandData.FridayCutoffTime),
        new Pair<Integer, Long>(-2, BandData.SaturdayCutoffTime),
        new Pair<Integer, Long>(-3, BandData.SundayCutoffTime),
    };

    public void setAdapter(SectionsPagerAdapter adapter) {
        mAdapter = adapter;
    }

    public void setThreePaneFrag(PanedTimetableFragment frag) {
        mThreePaneFrag = frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
            return mView;
        }

        mView = inflater.inflate(R.layout.timetable_section, container, false);
        Bundle bundle = getArguments();

        if (bundle != null)
            mKind = BandData.kindFromInt(bundle.getInt(ARG_SECTION_KIND));
        else
            mKind = BandData.Kind.BAND_PINE;

        mListView = (ListView) mView.findViewById(R.id.eventlist);
        mListView.setOnItemClickListener(this);

        if (mKind == BandData.Kind.CUSTOM)
            setEmptyView(inflater, mListView, container,
                         R.layout.empty_favorites);
        else
            setEmptyView(inflater, mListView, container,
                         R.layout.progress_indicator);

        new TimetableSectionLoader(this).execute("");

        if (mKind == BandData.Kind.CUSTOM)
            mData = new BandDataCollection<BandData>();

        return mView;
    }

    private void setEmptyView(LayoutInflater inflater, ListView list,
                              ViewGroup container, int layout) {
        View emptyView = inflater.inflate(layout, container, false);
        emptyView.setVisibility(View.GONE);
        ((ViewGroup) list.getParent()).addView(emptyView);
        list.setEmptyView(emptyView);
    }

    public BandData.Kind getKind() {
        return mKind;
    }

    public void reload(BandData[] newData) {
        if (getActivity() == null) {
            Festivity.log("getActivity() == null for TimetableSectionFragment " + mKind);
            return;
        }

        mData = new BandDataCollection<BandData>(newData);
        mListView.setAdapter(new EventListAdapter(this,
                                                  getResources().getColor(R.color.pastel_green),
                                                  mKind,
                                                  LayoutInflater.from(getActivity()),
                                                  getActivity()));
    }

    public void reload(BandData bd) {
        if (mKind == BandData.Kind.CUSTOM)
            updateOrRemove(bd);
        else
            mData.put(bd);

        EventListAdapter ela = (EventListAdapter) mListView.getAdapter();
        ela.reindex();
        ela.notifyDataSetChanged();
        mListView.invalidateViews();
    }

    private void updateOrRemove(BandData bd) {
        if (bd.isChecked()) {
            mData.put(bd);

            // XXX: new item in list, the list needs to be resorted. I don't
            // like it, but it will work for now.
            BandData[] arr = mData.sorted().toArray(new BandData[0]);
            mData = new BandDataCollection<BandData>(arr);
        }
        else
            mData.remove(bd.getId());
    }

    public ListView getListView() {
        return mListView;
    }

    private MjrApplication getApp() {
        return ((Festivity) getActivity()).getApp();
    }

    public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id) {
        EventListAdapter.ViewHolder holder
            = (EventListAdapter.ViewHolder) view.getTag();
        BandData bd = mData.get(holder.itemId);

        if (!bd.hasDetailsPage())
            return;

        if (mThreePaneFrag == null) {
            Intent intent = new Intent(getActivity(),
                                       EventDetailsActivity.class);
            intent.putExtra("band_id", bd.getId());
            startActivityForResult(intent, 0);
        } else {
            mThreePaneFrag.bandClicked(bd);
        }
    }

    public void checkItem(String id, boolean isChecked) {
        BandData bd = mData.get(id);

        if (bd == null) {
            Festivity.log("bd == null. BAD!");
            return;
        }

        bd.setIsChecked(isChecked);
        Utils.updateFavoriteItem(getActivity(), bd);

        if (mThreePaneFrag != null)
            mThreePaneFrag.reloadFavorites(bd);
        else
            mAdapter.reloadFavorites(bd);

        getApp().updateAlarmInfo(bd);
        getApp().setTimetableEntryToClear(bd);
    }

    private static class EventListAdapter extends BaseAdapter {
        private static final int TYPE_HEADING = 0;
        private static final int TYPE_ENTRY = 1;
        private LayoutInflater mInflater;
        private int[] mReindexer = null;
        private int mExtraRows = 0;
        private TimetableSectionFragment mFragment = null;
        private static int mHeadings[] = new int[] {
            R.string.day_friday,
            R.string.day_saturday,
            R.string.day_sunday,
        };
        private OnCheckedChangeListener mCheckListener = null;
        private BandData.Kind mKind;
        private Context mContext;
        private int mHeadingColor;

        public EventListAdapter(TimetableSectionFragment fragment,
                                int headingColor,
                                BandData.Kind kind,
                                LayoutInflater inflater,
                                Context context) {
            mFragment = fragment;
            mKind = kind;
            mContext = context;
            mHeadingColor = headingColor;
            mInflater = inflater;
            mExtraRows = 0;
            mCheckListener = new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    ViewHolder holder = (ViewHolder) buttonView.getTag();
                    mFragment.checkItem(holder.itemId, isChecked);
                }
            };

            if (mFragment.mData.isEmpty())
                return;

            reindex();
            Assert.assertTrue(mExtraRows > 0);
        }

        public void reindex() {
            BandData[] data = mFragment.mData.toArray();
            mReindexer = new int[data.length + 3];
            int dataIndex = 0;
            int reindexerIndex = 0;

            for (Pair<Integer, Long> day : mFestivalDays) {
                boolean firstIteration = true;

                while (dataIndex < data.length) {
                    long time = data[dataIndex].getTime();

                    if (time > day.second)
                        break;

                    if (firstIteration) {
                        mReindexer[reindexerIndex] = day.first;
                        ++reindexerIndex;
                        firstIteration = false;
                        continue;
                    }

                    mReindexer[reindexerIndex] = dataIndex;
                    ++reindexerIndex;
                    ++dataIndex;
                }
            }

            mExtraRows = reindexerIndex - dataIndex;
        }

        public int getCount() {
            return mFragment.mData.size() + mExtraRows;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public int getViewTypeCount() {
            return 2;
        }

        public int getItemViewType(int position) {
            if (mReindexer[position] < 0)
                return TYPE_HEADING;

            return TYPE_ENTRY;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (getItemViewType(position) == TYPE_HEADING)
                return getHeadingView(position);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.event_list_item,
                                                parent, false);
                holder = mkHolder(convertView);
                convertView.setTag(holder);
            }
            else
                holder = (ViewHolder) convertView.getTag();

            BandData[] data = mFragment.mData.toArray();
            BandData bd = data[mReindexer[position]];
            String name = bd.getName();
            String descr = bd.getDescr();

            if (!"".equals(descr))
                name = descr + " - " + name;

            String repr = bd.getTimeStr() + " - " + name;
            holder.text.setText(repr);
            holder.itemId = bd.getId();


            if (mKind == BandData.Kind.CUSTOM) {
                // This is a little bit of a hack, but it works... In the
                // Favorites view, we don't really want to have a checkbox, but
                // a simple icon, indicating the stage of the event. So make
                // the checkbox unclickable and set the drawable of that stage
                holder.favIcon.setClickable(false);
                holder.favIcon.setButtonDrawable(bd.getMarkerResId());
            }
            else {
                // Unset the listener, then mark the checkbox with the value
                // from the data set, then set the listener again. This avoids
                // excessive callbacks from the .setChecked() call instead of
                // real human interaction
                holder.favIcon.setOnCheckedChangeListener(null);
                holder.favIcon.setChecked(bd.isChecked());
                holder.favIcon.setOnCheckedChangeListener(mCheckListener);
            }

            holder.details.setVisibility(bd.hasDetailsPage()
                                         ? View.VISIBLE
                                         : View.INVISIBLE);

            return convertView;
        }

        @Override
        public boolean isEnabled(int position) {
            if (getItemViewType(position) == TYPE_HEADING)
                return false;

            return true;
        }

        private ViewHolder mkHolder(View convertView) {
            ViewHolder holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.favIcon = (CheckBox) convertView.findViewById(R.id.fav_icon);
            holder.details
                = (ImageView) convertView.findViewById(R.id.details_marker);
            holder.favIcon.setTag(holder);
            return holder;
        }

        private View getHeadingView(int position) {
            TextView tv = new TextView(mInflater.getContext());
            tv.setTypeface(null, Typeface.BOLD);
            tv.setGravity(Gravity.CENTER);
            tv.setHeight(48);
            tv.setBackgroundColor(mHeadingColor);
            int heading = -mReindexer[position] - 1;
            tv.setText(mHeadings[heading]);
            return tv;
        }

        static class ViewHolder {
            TextView text;
            CheckBox favIcon;
            ImageView details;
            String itemId;
        }
    }
}
