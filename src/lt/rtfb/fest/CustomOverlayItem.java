package lt.rtfb.fest;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class CustomOverlayItem extends OverlayItem
{
    private int mDrawable;

    public CustomOverlayItem (GeoPoint gp, String title, String info, int d)
    {
        super (gp, title, info);
        mDrawable = d;
    }

    public int getDrawableId ()
    {
        return mDrawable;
    }
}
