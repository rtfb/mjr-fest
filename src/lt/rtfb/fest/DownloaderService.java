package lt.rtfb.fest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.http.util.ByteArrayBuffer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class DownloaderService extends Service
{
    private final int FIVE_MINS_IN_MILLIS = 5 * 60 * 1000;
    private final IBinder mBinder = new LocalBinder ();
    private Thread mWorkerThread = null;
    private final boolean DEBUG_LOG = true;
    private final String THREAD_NAME = "DownloaderServiceWorkerTask";
    private List<String> mUrlQueue = null;
    private List<String> mLocalFilesQueue = null;
    private Context mContext;
    private long mNextCheck = -1;

    public class LocalBinder extends Binder
    {
        DownloaderService getService ()
        {
            return DownloaderService.this;
        }
    }

    public DownloaderService ()
    {
        mUrlQueue = new ArrayList<String> ();
        mLocalFilesQueue = new ArrayList<String> ();
        mContext = this;
    }

    public void addUrlToQueue (String url)
    {
        mUrlQueue.add (url);
        Festivity.log (String.format ("'%s' queued for download", url));
    }

    public void wakeUp ()
    {
        // Before waking up, reset the target wakeup time, so that we wouldn't
        // hit a safeguard for an unintended wakeup
        mNextCheck = System.currentTimeMillis ();
        synchronized (mBinder)
        {
            mBinder.notify ();
        }
    }

    public List<String> getLocalFiles ()
    {
        return mLocalFilesQueue;
    }

    @Override
    public void onCreate ()
    {
        Festivity.log ("DownloaderService.onCreate()");
        synchronized (mBinder)
        {
            if (mWorkerThread == null)
            {
                mWorkerThread = new Thread (null, mTask, THREAD_NAME);
                mWorkerThread.start ();
            }
        }
    }

    @Override
    public void onDestroy ()
    {
        Festivity.log ("DownloaderService.onDestroy()");
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId)
    {
        Festivity.log ("DownloaderService. Received start id "
                       + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    Runnable mTask = new Runnable ()
    {
        public void run ()
        {
            // This thread never stops itself. As long as the service is alive,
            // it always checks if there's anything to download
            while (true)
            {
                long currentTimeMillis = System.currentTimeMillis ();
                mNextCheck = pickNextWakeup (currentTimeMillis);

                if (currentTimeMillis >= mNextCheck)
                    continue;

                synchronized (mBinder)
                {
                    try
                    {
                        mBinder.wait (mNextCheck - currentTimeMillis);

                        if (DEBUG_LOG)
                        {
                            long id = Thread.currentThread ().getId ();
                            String name = Thread.currentThread ().getName ();
                            String m = String.format(Locale.US,
                                                     "thread wakeup, %d (%s)",
                                                     id, name);
                            Festivity.log (m);
                        }
                    }
                    catch (Exception e)
                    {}
                }
            }
        }
    };

    private long pickNextWakeup (long currentTimeMillis)
    {
        // We could've been wakened up by notify() or notifyAll(), so do an
        // extra check for time:
        if (currentTimeMillis < mNextCheck)
            return mNextCheck;

        long nextCheck = currentTimeMillis + FIVE_MINS_IN_MILLIS;

        if (!Utils.isOnline (mContext))
        {
            Festivity.log ("!isOnline(). Get back to sleeping.");
            return nextCheck;
        }

        if (mUrlQueue.isEmpty ())
        {
            Festivity.log ("mUrlQueue.isEmpty(). Get back to sleeping.");
            return nextCheck;
        }

        String url = mUrlQueue.remove (0);
        String localFile = download (url);

        if (localFile != null)
        {
            mLocalFilesQueue.add (localFile);
            Festivity.log (localFile + " downloaded.");
            processLocalFile (localFile);
        }
        else
        {
            Festivity.log (String.format ("Failed to download '%s'", url));
        }

        // If there's still things to download, don't sleep, start downloading
        // the next file immediately
        if (!mUrlQueue.isEmpty ())
            nextCheck = currentTimeMillis;

        return nextCheck;
    }

    @Override
    public IBinder onBind (Intent intent)
    {
        return mBinder;
    }

    private String download (String addr)
    {
        URL url = null;

        try
        {
            url = new URL (addr);
        }
        catch (MalformedURLException mue)
        {
            Festivity.log ("malformed url: " + mue);
            return null;
        }

        HttpURLConnection conn = null;

        try
        {
            conn = (HttpURLConnection) url.openConnection ();
            conn.connect ();

            InputStream is = conn.getInputStream ();
            BufferedInputStream bis = new BufferedInputStream (is, 8 * 1024);
            ByteArrayBuffer bab = new ByteArrayBuffer (4 * 1024);
            int current = 0;

            while ((current = bis.read ()) != -1)
            {
                bab.append ((byte)current);
            }

            bis.close ();

            if (bab.length () == 0)
            {
                Festivity.log ("empty buffer again! Damn!");
                int respCode = conn.getResponseCode ();
                Festivity.log ("HTTP response code = " + respCode);
                return null;
            }

            File f = new File (mContext.getCacheDir (), url.getFile ());
            return Utils.writeFile (f, bab.toByteArray ());
        }
        catch (IOException ex)
        {
            String m = String.format ("failed to wget '%s'", url);
            Festivity.log (m);
            return null;
        }
        finally
        {
            if (conn != null)
                conn.disconnect ();
        }
    }

    private void processLocalFile (String localFile)
    {
        if (localFile.endsWith (".xml"))
        {
            try
            {
                XmlPullParserFactory factory
                    = XmlPullParserFactory.newInstance ();
                factory.setNamespaceAware (true);
                XmlPullParser xpp = factory.newPullParser ();

                xpp.setInput (new StringReader (Utils.readFile (localFile)));
                int eventType = xpp.getEventType ();

                while (eventType != XmlPullParser.END_DOCUMENT)
                {
                    if (eventType == XmlPullParser.START_TAG
                        && xpp.getName ().equalsIgnoreCase ("image_url"))
                    {
                        String url = xpp.nextText ();

                        if (url.toLowerCase(Locale.US).startsWith("http"))
                            addUrlToQueue (url);
                    }

                    eventType = xpp.next ();
                }
            }
            catch (XmlPullParserException xppe)
            {
                Festivity.log (String.format ("Failed to parse '%s': %s",
                                              localFile, xppe));
            }
            catch (IOException ioe)
            {
                Festivity.log (String.format ("Error while parsing '%s': %s",
                                              localFile, ioe));
            }
        }
    }
}
