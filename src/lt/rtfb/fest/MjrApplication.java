package lt.rtfb.fest;

import android.app.Application;

public class MjrApplication extends Application
{
    private AlarmService mAlarmService = null;
    private BandData mDirtyTimetableEvent = null;
    private MapLegendOverlay mMapLegend = null;

    public void setAlarmService (AlarmService svc)
    {
        mAlarmService = svc;
    }

    public AlarmService getAlarmService ()
    {
        return mAlarmService;
    }

    public void updateAlarmInfo(BandData bd) {
        if (mAlarmService == null)
        {
            Festivity.log ("updateAlarmInfo: alarm service not connected yet.");
            return;
        }

        mAlarmService.updateItem(bd);
    }

    public void setTimetableEntryToClear (BandData bd)
    {
        mDirtyTimetableEvent = bd;
    }

    public BandData getTimetableEntryToClear ()
    {
        return mDirtyTimetableEvent;
    }

    public void setMapLegend (MapLegendOverlay legend)
    {
        mMapLegend = legend;
    }

    public void cancelMapToast ()
    {
        if (mMapLegend != null)
            mMapLegend.cancelToast ();
    }
}
