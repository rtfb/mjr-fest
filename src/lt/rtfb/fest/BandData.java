package lt.rtfb.fest;

import android.database.Cursor;
import java.util.Date;
import java.util.Locale;

public class BandData implements SelfHashable
{
    private String mName;
    private String mDescr;
    private boolean mShouldNotify;
    private String mId;
    private Kind mKind;
    private Date mDate;
    public static long FridayCutoffTime = new Date(112, 7, 25, 5, 0).getTime();
    public static long SaturdayCutoffTime = new Date(112, 7, 26, 5, 0).getTime();
    public static long SundayCutoffTime = new Date(112, 7, 27, 5, 0).getTime();
    private String mTimeStr;
    private String mImage;
    private boolean mHasDetails;
    private String mNameForDetails;
    private String mTextForDetails;

    public static final int STATUS_UNCHECKED = 0;
    public static final int STATUS_CHECKED = 1;
    public static final int STATUS_ALREADY_NOTIFIED = 2;

    public enum Kind
    {
        BAND_PINE,
        BAND_MAIN,
        BAND_NORD,
        LECTURE,
        CRAFT,
        OTHER,

        // That's a special one, it doesn't specify the nature of the event,
        // but the nature of an item in the app:
        CUSTOM
    }
    private static Kind[] mKindValues = Kind.values();

    public enum DayOfFest
    {
        THURS,
        FRI,
        SAT,
        SUN,
        AFTERPARTY
    }

    public static Kind kind (String kind)
    {
        if (kind.equals ("band_pine"))
            return Kind.BAND_PINE;
        else if (kind.equals ("band_main"))
            return Kind.BAND_MAIN;
        else if (kind.equals ("band_nord"))
            return Kind.BAND_NORD;
        else if (kind.equals ("craft"))
            return Kind.CRAFT;
        else if (kind.equals ("lecture"))
            return Kind.LECTURE;
        else if (kind.equals ("other"))
            return Kind.OTHER;
        else
            return Kind.CUSTOM;
    }

    public BandData (Cursor cursor)
    {
        mId = cursor.getString (0);
        mKind = kindFromInt(cursor.getInt(1));
        mDate = new Date(cursor.getLong(2) * 1000);
        mTimeStr = cursor.getString(3);
        mShouldNotify = cursor.getInt(4) == 1;
        mImage = cursor.getString(5);
        mName = cursor.getString(6);
        mDescr = cursor.getString(7);
        mHasDetails = cursor.getInt(8) != 0;
        mNameForDetails = cursor.getString(9);
        mTextForDetails = cursor.getString(10);
    }

    public static Kind kindFromInt(int k) {
        return mKindValues[k];
    }

    public Kind getKind ()
    {
        return mKind;
    }

    public int getMarkerResId() {
        switch (mKind) {
            case BAND_MAIN:
                return R.drawable.mainstage;
            case BAND_PINE:
                return R.drawable.pinestage;
            case BAND_NORD:
                return R.drawable.northstage;
            case CRAFT:
                return R.drawable.crafts;
            case LECTURE:
                return R.drawable.lecturetent;
            default:
                return R.drawable.info;
        }
    }

    public String getName ()
    {
        return mName;
    }

    public String getDescr ()
    {
        return mDescr;
    }

    public boolean hasDetailsPage() {
        return mHasDetails;
    }

    public String getId ()
    {
        return mId;
    }

    public String getTimeStr ()
    {
        return mTimeStr;
    }

    private static String getSelectFmt() {
        return "select e.id, e.kind, e.start_time, e.time_str, e.status,"
             + "       e.image, l10n.name, l10n.descr,"
             + "       l10n.has_details, l10n.name_for_details,"
             + "       l10n.text_for_details "
             + "from ttable_entry as e "
             + "left join ttable_entry_l10n as l10n "
             + "     on e.id = l10n.event_id "
             + "     and l10n.language_id = ("
             + DbHelper.getLangSelectSql()
             + ") %s order by e.start_time asc";
    }

    public static String getSelectSql(Kind kind) {
        String fmt = getSelectFmt();
        String where = "where e.status > 0";

        if (kind != Kind.CUSTOM)
            where = String.format(Locale.US,
                                  "where e.kind = %d", kind.ordinal());

        return String.format(fmt, where);
    }

    public static String getSelectSql(String hash) {
        String fmt = getSelectFmt();
        String where = String.format(Locale.US, "where e.id = '%s'", hash);
        return String.format(fmt, where);
    }

    public static String getSelectSql(int status) {
        String fmt = getSelectFmt();
        String where = String.format(Locale.US, "where e.status = %d", status);
        return String.format(fmt, where);
    }

    public int getStatus() {
        if (isChecked())
            return STATUS_CHECKED;
        else
            return STATUS_UNCHECKED;
    }

    public long getTime ()
    {
        return mDate.getTime ();
    }

    public void setIsChecked (boolean checked)
    {
        mShouldNotify = checked;
    }

    public boolean isChecked ()
    {
        return mShouldNotify;
    }

    public int day ()
    {
        long time = mDate.getTime ();

        if (time < FridayCutoffTime)
            return R.string.day_friday;

        if (time < SaturdayCutoffTime)
            return R.string.day_saturday;

        if (time > SaturdayCutoffTime)
            return R.string.day_sunday;

        return R.string.day_friday;
    }

    public String getImage() {
        return mImage;
    }

    public boolean hasDetails() {
        return mHasDetails;
    }

    public String getNameForDetails() {
        return mNameForDetails;
    }

    public String getTextForDetails() {
        return mTextForDetails;
    }
}
