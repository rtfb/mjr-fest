package lt.rtfb.fest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import java.util.HashMap;

public abstract class TabManager {
    protected FragmentActivity mFragmentActivity;
    protected MjrApplication mApp;
    protected int mContainerId;
    protected HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
    protected TabInfo mLastTabInfo = null;

    protected static class TabInfo {
        protected final String tag;
        protected final Class<?> clss;
        protected final Bundle bundle;
        protected Fragment fragment;

        public TabInfo(String tag, Class<?> clss, Bundle bundle) {
            this.tag = tag;
            this.clss = clss;
            this.bundle = bundle;
        }
    }

    public TabManager(FragmentActivity fragmentActivity, int containerId) {
        mFragmentActivity = fragmentActivity;
        mApp = (MjrApplication) mFragmentActivity.getApplication();
        mContainerId = containerId;
    }

    protected void createOrShow(FragmentTransaction xaction, TabInfo tab) {
        if (tab != null)
            if (tab.fragment == null) {
                String name = tab.clss.getName();
                tab.fragment = Fragment.instantiate(mFragmentActivity, name,
                                                    tab.bundle);
                xaction.add(mContainerId, tab.fragment, tab.tag);
            }
            else
                xaction.show(tab.fragment);
    }

    protected void cancelMapToast(String tabTag) {
        if (tabTag.equals(MapFragment.FRAGMENT_TAG))
            mApp.cancelMapToast();
    }

    protected void detach(TabInfo tabInfo) {
        FragmentManager fm = mFragmentActivity.getSupportFragmentManager();

        // Check to see if we already have a fragment for this tab, probably
        // from a previously saved state. If so, deactivate it, because our
        // initial state is that a tab isn't shown.
        tabInfo.fragment = fm.findFragmentByTag(tabInfo.tag);

        if (tabInfo.fragment != null && !tabInfo.fragment.isDetached()) {
            FragmentTransaction xaction = fm.beginTransaction();
            xaction.detach(tabInfo.fragment);
            xaction.commit();
        }
    }

    protected void switchTabByTag(String tag) {
        TabInfo newTabInfo = (TabInfo) mTabs.get(tag);

        if (mLastTabInfo != newTabInfo)
        {
            FragmentManager fm = mFragmentActivity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();

            if (mLastTabInfo != null)
                if (mLastTabInfo.fragment != null)
                    fragmentTransaction.hide(mLastTabInfo.fragment);

            createOrShow(fragmentTransaction, newTabInfo);

            mLastTabInfo = newTabInfo;
            fragmentTransaction.commit();
            fm.executePendingTransactions();
        }

        cancelMapToast(tag);
    }

    public abstract void init();
    public abstract String getCurrentTabTag();
    public abstract void setCurrentTabByTag(String tag);

    private TimetableFragment getTimetable() {
        FragmentManager fm = mFragmentActivity.getSupportFragmentManager();
        String tag = TimetableFragment.FRAGMENT_TAG;
        return (TimetableFragment) fm.findFragmentByTag(tag);
    }

    public int getTimetableSelection() {
        TimetableFragment tf = getTimetable();
        if (tf != null)
            return tf.getSelection();
        else
            return -1;
    }

    public void setTimetableSelection(int sel) {
        TimetableFragment tf = getTimetable();
        if (tf != null)
            tf.setSelection(sel);
    }

    public String getTimetableFragName() {
        return TimetableFragment.class.getSimpleName();
    }

    public TimetableSectionFragment getCurrTimetableSectionFrag() {
        TimetableFragment frag = getTimetable();
        return frag.getCurrFragment();
    }
}
