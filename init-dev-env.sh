#!/bin/sh

if [ `uname -m` = "x86_64" ] ; then
    sudo apt-get install --yes ia32-libs
fi

if ! [ `which javac` ] ; then
    sudo apt-get install --yes openjdk-7-jdk
fi

# This script is in the repo, so most likely we already have Git. But let's
# check just in case, maybe the source was obtained as tarball:
if ! [ `which git` ] ; then
    sudo apt-get install --yes git-core
fi

# XXX: will it install compatible version of ant??
if ! [ `which ant` ] ; then
    sudo apt-get install --yes ant
fi

if ! [ -d ~/android-sdk-linux ] ; then
    sdk=android-sdk_r21-linux.tgz
    wget -O /tmp/$sdk http://dl.google.com/android/$sdk
    tar -xzvvf /tmp/$sdk -C ~
    rm /tmp/$sdk
fi

packages="platform-tools,android-17,addon-google_apis-google-17"
~/android-sdk-linux/tools/android update sdk --no-ui --filter $packages

echo "sdk.dir=${HOME}/android-sdk-linux" > local.properties
