#/bin/env python

import os
import glob
from HTMLParser import HTMLParser
import json


data = {}


class MyHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.name = ''
        self.text = ''
        self.image = ''
        self.look_for_name = False
        self.in_body = False
        self.path = ''

    def handle_starttag(self, tag, attrs):
        #print "Encountered a start tag:", tag
        if tag == 'img':
            self.image = attrs[0][1]

        if tag == 'body':
            self.in_body = True

        if self.in_body:
            self.path += tag + '/'

    def handle_endtag(self, tag):
        #print "Encountered an end tag :", tag
        pass

    def handle_data(self, data):
        #print "Encountered some data  :", data
        if self.look_for_name:
            self.name = data
            self.look_for_name = False

        if 'INFO_PANEL' in data:
            self.look_for_name = True

        if self.path.startswith('body/div/div/img/div/div/p/'):
            #self.in_body = False
            #self.path = ''
            if data.strip() != '':
                self.text += '<p>' + data + '</p>'


def extract(html):
    filename = os.path.split(html)[-1]
    key = os.path.splitext(filename)[0]

    #if html.endswith('vijas-lt.html'):
        #import pdb
        #pdb.set_trace()

    if key.endswith('-lt'):
        key = key[:-3]

    parser = MyHTMLParser()
    parser.feed(''.join(open(html).readlines()))
    #print('%s (%s):\nTXT:%s' % (parser.name, parser.image, parser.text))

    if key in data:
        obj = data[key]
        lt = obj['lt']
        en = obj['en']
    else:
        lt = ''
        en = ''

    name = parser.name
    image = parser.image

    noext = os.path.splitext(filename)[0]
    if noext.endswith('-lt'):
        lt = parser.text
    else:
        en = parser.text

    data[key] = {'name': name, 'lt': lt, 'en': en, 'img': image}


def main():
    htmls = glob.glob('old-data/*.html')
    #print(len(htmls))

    cl = []
    for i in htmls:
        z = os.path.split(i)[1]
        f = os.path.splitext(z)[0]
        if f.endswith('-lt'):
            f = f[:-3]
        cl.append(f)
    clset = set(cl)
    #print(len(clset))

    #extract('assets/atl_skyle-lt.html')
    for html in htmls:
        if 'dummy' in html:
            continue
        extract(html)

    #print(len(data))
    #print(data.keys())
    print('set difference: ' + str(clset.difference(set(data.keys()))))
    open('details.json', 'w').write(json.dumps(data,
                                               sort_keys=True,
                                               indent=4,
                                               separators=(',', ': ')))


if __name__ == '__main__':
    main()
