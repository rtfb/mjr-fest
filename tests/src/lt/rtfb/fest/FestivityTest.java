package lt.rtfb.fest;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.ListView;
import com.jayway.android.robotium.solo.Solo;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class lt.rtfb.fest.FestivityTest \
 * lt.rtfb.fest.tests/android.test.InstrumentationTestRunner
 */
public class FestivityTest extends ActivityInstrumentationTestCase2<Festivity> {
    private Festivity mActivity;
    private Solo mSolo;

    public FestivityTest() {
        super("lt.rtfb.fest", Festivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
        mSolo = new Solo(getInstrumentation(), getActivity());
    }

    protected void tearDown() throws Exception {
        try {
            mSolo.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }

        getActivity().finish();
        super.tearDown();
    }

    @UiThreadTest
    public void testWelcomeTabDoesntCrash() {
        switchTabByTagUi(WelcomeFragment.FRAGMENT_TAG);
    }

    @UiThreadTest
    public void testTimetableTabDoesntCrash() {
        switchTabByTagUi(mActivity.getTabManager().getTimetableFragName());
    }

    @UiThreadTest
    public void testMapTabDoesntCrash() {
        switchTabByTagUi(MapFragment.FRAGMENT_TAG);
    }

    private void switchTabByTagUi(String tag) {
        mActivity.getTabManager().setCurrentTabByTag(tag);
    }

    private void switchTabByTag(final String tag) {
        mActivity.runOnUiThread(
            new Runnable() {
                public void run() {
                    switchTabByTagUi(tag);
                }
            });
    }

    private void setTimetableSelection(final int tab) {
        mActivity.runOnUiThread(
            new Runnable() {
                public void run() {
                    mActivity.getTabManager().setTimetableSelection(tab);
                }
            });
    }

    public void testActivityPreservesTab() {
        final String tab = MapFragment.class.getSimpleName();
        switchTabByTag(tab);

        mActivity.finish();
        setActivity(null);
        assertEquals(tab, getActivity().getTabManager().getCurrentTabTag());
    }

    public void testSettingsLaunchFine() {
        mSolo.assertCurrentActivity("wrong activity", Festivity.class);

        if (android.os.Build.VERSION.SDK_INT < 11)
            mSolo.pressMenuItem(0);
        else
            mSolo.clickOnActionBarItem(R.id.menu_settings);

        mSolo.assertCurrentActivity("wrong activity", SettingsActivity.class);
        mSolo.goBack();
    }

    public void testEventDetailsActivity() {
        mSolo.assertCurrentActivity("wrong activity", Festivity.class);
        switchTabByTag(mActivity.getTabManager().getTimetableFragName());
        getInstrumentation().waitForIdleSync();
        assertNotNull(mActivity);
        setTimetableSelection(1);
        mSolo.clickOnText("UGNIAVIJAS");
        mSolo.waitForFragmentByTag(EventDetailsFragment.tagName());
    }

    // XXX: this test is broken on PanedTimetableFragment and flip-flops
    // elsewhere
    public void testCheckmarkAddsItemToFavs() {
        mSolo.assertCurrentActivity("wrong activity", Festivity.class);
        switchTabByTag(mActivity.getTabManager().getTimetableFragName());
        getInstrumentation().waitForIdleSync();
        setTimetableSelection(0);
        String emptyFavsText = "Touch the birds next to things you want to see";
        assertTrue(mSolo.searchText(emptyFavsText));
        setTimetableSelection(1);
        getInstrumentation().waitForIdleSync();

        // Click on the first available checkbox (that will be the first band
        // in the list)
        mSolo.clickOnCheckBox(0);

        getInstrumentation().waitForIdleSync();
        setTimetableSelection(0);
        getInstrumentation().waitForIdleSync();
        TabManager tabMgr = getActivity().getTabManager();
        TimetableSectionFragment tsf = tabMgr.getCurrTimetableSectionFrag();
        ListView favs = tsf.getListView();

        // Count-1 because the 0th item is header view:
        assertEquals(1, favs.getAdapter().getCount() - 1);

        // Now click on the fav item and uncheck, to clean up:

        // For some reason, Robotium thinks there are two lists available on
        // screen at this point. So use the 1st one (0-based). Click on a 2nd
        // line (1-based) in that list because the first one is the header.
        mSolo.clickInList(2, 1);

        mSolo.waitForFragmentByTag(EventDetailsFragment.tagName());
        mSolo.clickOnCheckBox(0);
        mSolo.goBack();
        assertTrue(mSolo.searchText(emptyFavsText));
    }
}
