package lt.rtfb.fest;

import android.test.AndroidTestCase;
import junit.framework.TestCase;

public class UtilTests extends TestCase {
    public void testFragmentTags() {
        assertEquals("WelcomeFragment", WelcomeFragment.FRAGMENT_TAG);
        assertEquals("TimetableFragment", TimetableFragment.FRAGMENT_TAG);
        assertEquals("MapFragment", MapFragment.FRAGMENT_TAG);
    }
}
