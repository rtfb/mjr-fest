#/bin/env python

import binascii
import datetime
import glob
import json
import os
import sqlite3
import time
import zipfile
from xml.dom.minidom import parse


global lt_lang_id
global en_lang_id


def create_schema(cursor):
    cursor.execute('''
        create table languages
        (id INTEGER primary key autoincrement,
         language CHARACTER(2) not null)''')
    cursor.execute('''
        create table ttable_entry_l10n
        (event_id TEXT not null references ttable_entry(id) on delete cascade on update cascade,
         language_id INTEGER not null references languages(id) on delete cascade on update cascade,
         name TEXT not null,
         descr TEXT not null,
         has_details INTEGER not null,
         name_for_details TEXT not null,
         text_for_details TEXT not null)''')
    cursor.execute('''
        create index l10n_ids on ttable_entry_l10n (event_id)''')
    # ttable_entry.status is one of BandData.STATUS_*
    cursor.execute('''
        create table ttable_entry (id TEXT PRIMARY KEY,
                                   kind INTEGER,
                                   start_time LONG,
                                   time_str CHARACTER(5),
                                   status INTEGER,
                                   image TEXT)''')
    cursor.execute('''
        create index ttable_kind_idx on ttable_entry (kind)''')


def prepopulate(cursor):
    global lt_lang_id
    global en_lang_id
    cursor.execute('insert into languages (language) values (\'lt\')')
    lt_lang_id = cursor.lastrowid
    cursor.execute('insert into languages (language) values (\'en\')')
    en_lang_id = cursor.lastrowid


def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)


def mktime(timestamp):
    tm = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M')
    return time.mktime(tm.timetuple())


def insert_event(c, kind, name, name_en, longname, longname_en,
                 timestamp, json):
    insert_sql = '''
        insert into ttable_entry(id, kind, start_time, time_str, status, image)
        values (?, ?, ?, ?, ?, ?)'''
    kind_to_int = {
        'band_pine': 0,
        'band_main': 1,
        'band_nord': 2,
        'lecture': 3,
        'craft': 4,
        'other': 5,
    }
    # XXX: relying on the representation of band's name is not bullet proof.
    # What if a typo gets fixed, for example?
    blob = bytearray(name + longname + timestamp, 'utf-8')
    event_id = 'id_%08x' % (binascii.crc32(blob) & 0xffffffff)
    img = 'nopic'
    if json:
        img = json['img']
    c.execute(insert_sql, (event_id, kind_to_int[kind],
                           mktime(timestamp),
                           timestamp[-5:], 0, img))
    insert_l10n_sql = '''
        insert into ttable_entry_l10n(event_id, language_id, name, descr,
                                      has_details, name_for_details,
                                      text_for_details)
        values (?, ?, ?, ?, ?, ?, ?)'''
    name_for_details = ''
    lt_details = ''
    en_details = ''
    if json:
        name_for_details = json['name']
        lt_details = json['lt']
        en_details = json['en']
    c.execute(insert_l10n_sql, (event_id, lt_lang_id,
                                name, longname,
                                lt_details != '',
                                name_for_details,
                                lt_details))
    if name_en == '':
        name_en = name
    if longname_en == '':
        longname_en = longname
    c.execute(insert_l10n_sql, (event_id, en_lang_id,
                                name_en, longname_en,
                                en_details != '',
                                name_for_details,
                                en_details))


def getData(entry, elem):
    data = entry.getElementsByTagName(elem)
    if data:
        return getText(data[0].childNodes)

    if elem.endswith('_en'):
        data = entry.getElementsByTagName(elem[:-3])
        if not data:
            return ''
        return getText(data[0].childNodes)

    return ''


def migrate_xml(cursor, dom, json):
    entries = dom.getElementsByTagName('entry')
    print('#entries = %d' % len(entries))
    for e in entries:
        html = getData(e, 'details_url')
        html_noext = html[:-5]
        insert_event(cursor, e.attributes['kind'].nodeValue,
                     getData(e, 'shortname'),
                     getData(e, 'shortname_en'),
                     getData(e, 'longname'),
                     getData(e, 'longname_en'),
                     getData(e, 'datetime'),
                     json[html_noext] if html_noext in json else None)


def read_xml(filename):
    return parse(open(filename))


def read_json(filename):
    return json.loads(open(filename).read())


def create_zip(dbname, zipname):
    with zipfile.ZipFile(zipname, 'w') as dbzip:
        dbzip.write(dbname, dbname, zipfile.ZIP_DEFLATED)


def main():
    db_filename = 'timetable.db'
    assets = 'assets'
    if os.path.exists(db_filename):
        os.unlink(db_filename)
    conn = sqlite3.connect(db_filename)
    c = conn.cursor()
    create_schema(c)
    prepopulate(c)
    json = read_json('details.json')
    migrate_xml(c, read_xml('timetable.xml'), json)
    conn.commit()
    create_zip(db_filename, os.path.join(assets, db_filename + '.zip'))


if __name__ == '__main__':
    main()
